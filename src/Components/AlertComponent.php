<?php

namespace Hachim\HHQuiz\Components;


use Illuminate\View\Component;
use Illuminate\Http\Request;

class AlertComponent extends Component
{
    /**
     * The alert type.
     *
     * @var string
     */
    public $type;

    /**
     * The alert message.
     *
     * @var string
     */
    public $message;

    /**
     * Create the component instance.
     *
     * @param  string  $type
     * @param  string  $message
     * @return void
     */
    public function __construct($type, $message, Request $request)
    {
        $this->type = $type;
        $this->message = $message;
        //dd($request->all());
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('hhquiz::components.alert');
    }
}