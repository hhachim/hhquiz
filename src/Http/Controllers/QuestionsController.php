<?php

namespace Hachim\HHQuiz\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hachim\HHQuiz\Models\Question\Question;
use Hachim\HHQuiz\Models\Question\Group;
use Hachim\HHQuiz\Models\Quiz\Quiz;
use Hachim\HHQuiz\Models\Quiz\Question as QuizQuestion;
use App\User;
use Illuminate\Support\Facades\Auth;

class QuestionsController extends Controller
{
    /**
     * @return View
     */
    public function index(Request $request)
    {
        $group = null;
        $groupPath = null;
        $answerType = $request->get('answer_type', null);
        $query = Question::currentUser()->with('author')->with('group');
        $me = Auth::user();
        $authorId = $me->getKey();
        if (!empty($request->get('author_id'))) {
            $authorId = (int) $request->get('author_id');
        }

        $query->where('author_id', $authorId);

        if (!empty($answerType)) {
            $query->where('answer_type', $answerType);
        }

        if (!empty($request->get('question_group')) && is_numeric($request->get('question_group'))) {
            $questionGroup = (int) $request->get('question_group');
            //dd($questionGroup);
            $query->parentGroup($questionGroup);
            $parentGroups = Group::parentsOf($questionGroup)->get();
            //dd($parentGroups);
            $group = Group::currentUser()->whereId($questionGroup)->with('parent')->first();

            $groupPath = $group->title;
            foreach ($parentGroups as $parent) {
                if ($parent->id == $group->id) {
                    continue;
                }
                $groupPath = $parent->title  . " / " . $groupPath;
            }
            $group->path = $groupPath;
        }

        $authors = User::currentUser()
            ->whereRaw('id in (SELECT distinct author_id FROM hhquiz_question) and id != ' . $me->getKey())
            ->get();

        $questions = $query->simplePaginate(config('hhquiz.pagination.itemsPerPage'));
        //dd($quizzes);
        //$this->alertSuccess('success message');
        \Session::flash('message', 'This is a message!');
        \Session::flash('alert-type', 'danger'); //success, danger, warning...
        $viewData = compact(
            'questions',
            'group',
            'authors',
            'me',
            'groupPath',
            'authorId',
            'answerType'
        );

        return view('hhquiz::questions.index', $viewData);
    }

    public function createForm()
    {
        $groups = Group::currentUser()->with('author')->with('parent')->get();

        return view('hhquiz::questions.create', compact('groups'));
    }

    public function create(Request $request)
    {
        $quizUuid = $request->get('quiz_uuid', null);
        if (!empty($quizUuid)) {
            //if not quiz found for this question, then ignore the _quiz_id value
            $Quiz = Quiz::currentUser()->whereUuid($quizUuid)->first();
            if ($Quiz == null) {
                $quizUuid = null;
            }
        }
        $request->validate(
            [
                'title' => 'required|max:45',
            ]
        );

        $isEnabled = 0;
        if (in_array($request->get('is_enabled'), [1, true, 'on'])) {
            $isEnabled = 1;
        }

        $data = [
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'keywords' => $request->get('keywords'),
            'answer_type' => $request->get('answer_type'),
            'is_enabled' => $isEnabled,
        ];

        $groupId = (int) $request->get('group_id');
        if (!empty($groupId)) {
            $data['group_id'] = $groupId;
        }

        $question = new Question($data);
        $question->save();

        if (!empty($quizUuid)) {
            $quizQuestionData = [
                'question_id' => $question->id,
                'quiz_id' => $Quiz->id,
            ];
            $quizQuestion = new QuizQuestion($quizQuestionData);
            $quizQuestion->save();

            return redirect(route('hhquiz.quizzes.get.one', $quizUuid))->with('success', 'Contact saved!');
        }

        return redirect(route('hhquiz.questions.get.one', $question->uuid))->with('success', 'Contact saved!');
    }

    public function getOne($uuid, Request $request)
    {
        $question = Question::currentUser()
            ->whereUuid($uuid)
            ->with('author')
            ->with('choices')
            ->with('quizzes')
            ->first();
        if ($question == null) {
            abort(404);
        }

        return view('hhquiz::questions.show', compact('question'));
    }

    public function updateForm($uuid)
    {
        $question = Question::currentUser()->whereUuid($uuid)->first();
        if ($question == null) {
            abort(404);
        }

        $groups = Group::currentUser()
            ->with('author')
            ->with('parent')
            ->get();

        return view('hhquiz::questions.update', compact('question', 'groups'));
    }

    public function update($uuid, Request $request)
    {
        $question = Question::currentUser()->whereUuid($uuid)->first();
        if ($question == null) {
            abort(404);
        }

        $request->validate(
            [
                'title' => 'required|max:45',
            ]
        );

        $question->title = $request->get('title');

        $isEnabled = 0;
        if (in_array($request->get('is_enabled'), [1, true, 'on'])) {
            $isEnabled = 1;
        }

        $question->is_enabled = $isEnabled;
        $question->content = $request->get('content');
        $question->keywords = $request->get('keywords');
        $groupId = (int) $request->get('group_id');
        if (!empty($groupId)) {
            $question->group_id = $groupId;
        } else {
            $question->group_id = null;
        }

        if (in_array($request->get('answer_type'), ['shortanswers', 'multichoices'])) {
            $question->answer_type = $request->get('answer_type');
        }
        $question->save();

        return redirect(route('hhquiz.questions.get.one', $question->uuid))->with('success', 'Contact saved!');
    }
}
