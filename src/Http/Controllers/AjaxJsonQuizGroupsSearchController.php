<?php

namespace Hachim\HHQuiz\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AjaxJsonQuizGroupsSearchController extends Controller
{
    public function __invoke(Request $request)
    {
        /*if (!$request->ajax()) {
            abort(404);
        }*

        $this->validate(
            $request,
            [
                'email' => 'bail|required|email',
                'message' => 'bail|required|max:250',
            ]
        );*/

        $groupKeysStr = $request->get('query', null);
        $groupKeysStr = str_replace('/', '', $groupKeysStr);
        if (empty($groupKeysStr) || strlen($groupKeysStr) < 2) {
            /*
            Attention la concaténation " / " est utilisé dans dans certains endroits comme
            sur la page /questions.
            Changer cette concaténatin peut impacter des bugs en cascades (penser à les ajuster)
            */
            $sqlQuery = 'WITH RECURSIVE tree ( data , id , level , path) AS
            (
         
                SELECT
                    title, CAST(id AS CHAR(50)) AS id , 0 ,  CAST(title AS CHAR(200)) AS path
            FROM hhquiz_quiz_group
                WHERE parent_id IS NULL
         
            UNION ALL
         
            SELECT
                    CHILD.title , CHILD.id , t.level + 1 ,  concat(CAST(t.path AS CHAR), " / " , CAST( CHILD.title AS CHAR) )
            FROM hhquiz_quiz_group AS CHILD
            INNER JOIN tree AS t
                    ON t.id = CHILD.parent_id
         
            )
         
        SELECT data , id , level, path
        FROM tree
        ORDER BY path
        LIMIT 1000
                         ';
            $groups = \DB::select($sqlQuery, array(Auth::user()->getKey()));

            return response()->json((array) $groups);
        }
        $groupKeysArr = explode(' ', $groupKeysStr);
        $likeStr = 'path LIKE ' . \DB::connection()->getPdo()->quote("%{$groupKeysArr[0]}%");
        for ($i = 1; $i < count($groupKeysArr); ++$i) {
            $likeStr .= ' AND path LIKE ' . \DB::connection()->getPdo()->quote("%{$groupKeysArr[$i]}%");
        }

        $sqlQuery = "
        WITH RECURSIVE tree ( data , id , level , path) AS
    (
 
        SELECT
            title, CAST(id AS CHAR(50)) AS id , 0 ,  CAST(title AS CHAR(200)) AS path
    FROM hhquiz_quiz_group
        WHERE parent_id is NULL
 
    UNION ALL
 
    SELECT
            CHILD.title , CHILD.id , t.level + 1 ,  concat(CAST(t.path AS CHAR), ' / ' , CAST( CHILD.title AS CHAR) )
    FROM hhquiz_quiz_group AS CHILD
    INNER JOIN tree AS t
            ON t.id = CHILD.parent_id
 
    )
 
SELECT data , id , level, path
FROM tree
WHERE $likeStr 
ORDER BY path
        ";
        //echo $sqlQuery;
        //die;
        $groups = \DB::select($sqlQuery);

        return response()->json((array) $groups);
    }
}
