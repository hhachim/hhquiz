<?php

namespace Hachim\HHQuiz\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hachim\HHQuiz\Models\Question\Group;

class QuestionGroupsController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $query = Group::currentUser()->with('author')->with('parent');
        $groups = $query->simplePaginate(config('hhquiz.pagination.itemsPerPage'));

        //dd($quizzes);
        //$this->alertSuccess('success message');
        \Session::flash('message', 'This is a message!');
        \Session::flash('alert-type', 'danger'); //success, danger, warning...

        //event(new BreadAdded($dataType, $data));
        //die("wip HHQuiz/IndexController");
        return view('hhquiz::questions.groups.index', compact('groups'));
    }

    public function createForm()
    {
        $parents = Group::currentUser()->with('author')->get();

        return view('hhquiz::questions.groups.create', compact('parents'));
    }

    public function create(Request $request)
    {
        $request->validate(
            [
                'title' => 'required|max:45',
            ]
        );

        $isEnabled = 0;
        if (in_array($request->get('is_enabled'), [1, true, 'on'])) {
            $isEnabled = 1;
        }

        $data = [
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'is_enabled' => $isEnabled,
        ];
        $parentId = (int) $request->get('parent_id');
        if (!empty($parentId)) {
            $data['parent_id'] = $parentId;
        }

        $group = new Group($data);
        $group->save();

        return redirect(route('hhquiz.questions.groups.index'))->with('success', 'Contact saved!');
    }

    public function getOne($uuid, Request $request)
    {
        $group = Group::currentUser()->whereUuid($uuid)->with('author')->first();
        if ($group == null) {
            abort(404);
        }

        $parents = Group::parentsOf($group->id)->get();

        return view('hhquiz::questions.groups.show', compact('group', 'parents'));
    }

    public function updateForm($uuid)
    {
        $group = Group::currentUser()->whereUuid($uuid)->first();
        if ($group == null) {
            abort(404);
        }
        $parents = Group::currentUser()->with('author')->where('id', '!=', $group->id)->get();

        return view('hhquiz::questions.groups.update', compact('group', 'parents'));
    }

    public function update($uuid, Request $request)
    {
        $group = Group::currentUser()->whereUuid($uuid)->first();
        if ($group == null) {
            abort(404);
        }

        $request->validate(
            [
                'title' => 'required|max:45',
            ]
        );

        $group->title = $request->get('title');

        $isEnabled = 0;
        if (in_array($request->get('is_enabled'), [1, true, 'on'])) {
            $isEnabled = 1;
        }

        $group->is_enabled = $isEnabled;
        $group->description = $request->get('description');
        $parentId = (int) $request->get('parent_id');
        if (!empty($parentId)) {
            $group->parent_id = $parentId;
        }
        $group->save();

        return redirect(route('hhquiz.questions.groups.index'))->with('success', 'Contact saved!');
    }
}
