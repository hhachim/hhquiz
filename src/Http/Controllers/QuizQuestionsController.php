<?php

namespace Hachim\HHQuiz\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hachim\HHQuiz\Models\Question\Group as QuestionGroup;
use Hachim\HHQuiz\Models\Quiz\Quiz;

class QuizQuestionsController extends Controller
{
    public function createForm($quizId, Request $request)
    {
        $quiz = Quiz::currentUser()->whereId($quizId)->first();
        if ($quiz == null) {
            abort(404);
        }

        $groups = QuestionGroup::currentUser()
            ->with('author')
            ->with('parent')
            ->get();

        return view('hhquiz::quizzes.questions.create', compact('groups', 'quiz'));
    }
}
