<?php

namespace Hachim\HHQuiz\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hachim\HHQuiz\Models\Quiz\Quiz;

class QuizzesController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $quizzes = Quiz::currentUser()->with('author')->get();
        //dd($quizzes);
        //$this->alertSuccess('success message');
        \Session::flash('message', 'This is a message!');
        \Session::flash('alert-type', 'danger'); //success, danger, warning...

        //event(new BreadAdded($dataType, $data));
        //die("wip HHQuiz/IndexController");
        return view('hhquiz::student.quizzes.index', compact('quizzes'));
    }

    public function getOne($id, Request $request)
    {
        $quiz = Quiz::currentUser()
            ->whereId($id)
            ->with('author')
            ->with('questions')
            ->first();
        if ($quiz == null) {
            abort(404);
        }

        return view('hhquiz::quizzes.show', compact('quiz'));
    }
}
