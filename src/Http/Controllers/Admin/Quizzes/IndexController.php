<?php

namespace Hachim\HHQuiz\Http\Controllers\Admin\Quizzes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hachim\HHQuiz\Models\Quiz\Quiz;

class IndexController extends Controller
{

    /**
     * @return View
     */
    public function index()
    {
        return view('hhquiz::admin.quizzes.index');
    }

    /**
     * @return View
     */
    public function indexAjax()
    {
        $sqlQuery = "
        WITH quizzes_cte ( id, nb_questions) AS (
            SELECT qz.id, count(*) as nb_questions
            FROM `hhquiz_quiz` qz
            LEFT JOIN hhquiz_quiz_question q ON qz.id = q.quiz_id 
            LEFT JOIN users u on qz.author_id = u.id
            WHERE 1
            GROUP BY qz.id
            )
            SELECT cte.nb_questions, qz2.*, u.name as author_name, '1' as classroom_id, 'quiz groupe title' as classroom_title 
            from quizzes_cte cte
            LEFT JOIN hhquiz_quiz qz2 ON cte.id = qz2.id
            LEFT JOIN users u on qz2.author_id = u.id
        ";
        $quizzes = \DB::select($sqlQuery);

        $result = [
            "total" => count($quizzes),
            "rows" => (array) $quizzes
        ];
        return response()->json($result);
    }

    public function getOne($uuid, Request $request)
    {
        $quiz = Quiz::currentUser()
            ->whereUuid($uuid)
            ->with('author')
            ->with('questions')
            ->first();
        if ($quiz == null) {
            abort(404);
        }

        return view('hhquiz::admin.quizzes.show', compact('quiz'));
    }

    public function createForm()
    {
        $parents = Quiz::currentUser()->get();

        return view('hhquiz::admin.quizzes.create', compact('parents'));
    }

    public function updateForm($uuid)
    {
        $quiz = Quiz::currentUser()->whereUuid($uuid)->first();
        if ($quiz == null) {
            abort(404);
        }

        $parents = Quiz::currentUser()->where('id', '!=', $quiz->id)->get();

        return view('hhquiz::admin.quizzes.update', compact('quiz', 'parents'));
    }

    public function update($uuid, Request $request)
    {
        $quiz = Quiz::currentUser()->whereUuid($uuid)->first();
        if ($quiz == null) {
            abort(404);
        }

        $request->validate(
            [
                'title' => 'required|max:45',
            ]
        );

        $quiz->title = $request->get('title');

        $isEnabled = 0;
        if (in_array($request->get('is_enabled'), [1, true, 'on'])) {
            $isEnabled = 1;
        }

        $failedQuestions = 0;
        if (in_array($request->get('select_user_failed_questions_first'), [1, true, 'on'])) {
            $failedQuestions = 1;
        }

        $quiz->is_enabled = $isEnabled;
        $quiz->select_user_failed_questions_first = $failedQuestions;
        $quiz->description = $request->get('description');
        $quiz->required_quiz_id = $request->get('required_quiz_id');
        $quiz->required_quiz_percent_note = $request->get('required_quiz_percent_note');
        $quiz->save();

        return redirect(route('hhquiz.quizzes.get.one', $quiz->uuid))->with('success', 'Contact saved!');
    }

    public function create(Request $request)
    {
        $request->validate(
            [
                'title' => 'required|max:45',
            ]
        );

        $isEnabled = 0;
        if (in_array($request->get('is_enabled'), [1, true, 'on'])) {
            $isEnabled = 1;
        }

        $failedQuestions = 0;
        if (in_array($request->get('select_user_failed_questions_first'), [1, true, 'on'])) {
            $failedQuestions = 1;
        }

        $quiz = new Quiz(
            [
                'title' => $request->get('title'),
                'description' => $request->get('description'),
                'is_enabled' => $isEnabled,
                'select_user_failed_questions_first' => $failedQuestions,
            ]
        );
        $requiredQuiz = $request->get('required_quiz_id', null);
        $percentNote = $request->get('required_quiz_percent_note', null);
        if (isset($requiredQuiz)) {
            $quiz->required_quiz_percent_note = $percentNote;
            $quiz->required_quiz_id = $requiredQuiz;
        }
        $quiz->required_quiz_id = $request->get('required_quiz_id');

        $quiz->save();

        return redirect(route('hhquiz.quizzes.get.one', $quiz->id))->with('success', 'Contact saved!');
    }
}
