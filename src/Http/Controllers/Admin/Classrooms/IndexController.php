<?php

namespace Hachim\HHQuiz\Http\Controllers\Admin\Classrooms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hachim\HHQuiz\Models\Classroom\Classroom;

class IndexController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        return view('hhquiz::admin.classrooms.index');
    }

    /**
     * @return View
     */
    public function indexAjax()
    {
        $classrooms = Classroom::currentUser()->with('author')->orderBy('order', 'ASC')->get()->toArray();
        $result = [
            "total" => count($classrooms),
            "rows" => (array) $classrooms
        ];
        return response()->json($result);
    }
}
