<?php

namespace Hachim\HHQuiz\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        return view('hhquiz::admin.home');
    }
}
