<?php

namespace Hachim\HHQuiz\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hachim\HHQuiz\Models\Question\Question;
use Hachim\HHQuiz\Models\Question\Choice;

class QuestionChoicesController extends Controller
{
    public function createForm($questionUuid)
    {
        $question = Question::currentUser()
            ->whereUuid($questionUuid)
            ->with('author')
            ->first();
        if ($question == null) {
            abort(404);
        }

        return view('hhquiz::questions.choices.create', compact('question'));
    }

    public function create($questionUuid, Request $request)
    {
        $question = Question::currentUser()
            ->whereUuid($questionUuid)
            ->with('author')
            ->first();
        if ($question == null) {
            abort(404);
        }

        $isGoodAnswer = 0;
        if (in_array($request->get('is_good_answer'), [1, true, 'on'])) {
            $isGoodAnswer = 1;
        }

        $data = [
            'content' => $request->get('content'),
            'feedback' => $request->get('feedback'),
            'is_good_answer' => $isGoodAnswer,
            'question_id' => $question->id,
        ];

        $choice = new Choice($data);
        $choice->save();

        return redirect(route('hhquiz.questions.get.one', $question->uuid))->with('success', 'Contact saved!');
    }

    /*public function getOne($uuid, Request $request)
    {
        $group = Group::currentUser()->whereId($id)->with('author')->first();
        if ($group == null) {
            abort(404);
        }

        return view('hhquiz::questions.groups.show', compact('group'));
    }*/

    public function updateForm($questionUuid, $choiceUuidd)
    {
        $question = Question::currentUser()
            ->whereUuid($questionUuid)
            ->with('author')
            ->first();
        if ($question == null) {
            abort(404);
        }

        $choice = Choice::currentUser()
            ->whereUuid($choiceUuidd)
            ->whereQuestionId($question->id)
            ->first();
        if ($choice == null) {
            abort(404);
        }

        return view('hhquiz::questions.choices.update', compact('question', 'choice'));
    }

    public function update($questionUuid, $choiceUuid, Request $request)
    {
        $question = Question::currentUser()
            ->whereUuid($questionUuid)
            ->with('author')
            ->first();
        if ($question == null) {
            abort(404);
        }

        $choice = Choice::currentUser()
            ->whereUuid($choiceUuid)
            ->whereQuestionId($question->id)
            ->first();
        if ($choice == null) {
            abort(404);
        }

        $isGoodAnswer = 0;
        if (in_array($request->get('is_good_answer'), [1, true, 'on'])) {
            $isGoodAnswer = 1;
        }
        $choice->is_good_answer = $isGoodAnswer;
        $choice->content = $request->get('content');
        $choice->feedback = $request->get('feedback');
        $choice->save();

        return redirect(route('hhquiz.questions.get.one', $question->uuid))->with('success', 'Contact saved!');
    }
}
