<?php

namespace Hachim\HHQuiz\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hachim\HHQuiz\Models\User\Group;

class GroupsController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $groups = Group::currentUser()->with('author')->get();
        //dd($quizzes);
        //$this->alertSuccess('success message');
        \Session::flash('message', 'This is a message!');
        \Session::flash('alert-type', 'danger'); //success, danger, warning...

        //event(new BreadAdded($dataType, $data));
        //die("wip HHQuiz/IndexController");
        return view('hhquiz::groups.index', compact('groups'));
    }

    public function createForm()
    {
        $parents = Group::currentUser()->with('author')->with('parent')->get();

        return view('hhquiz::groups.create', compact('parents'));
    }

    public function create(Request $request)
    {
        $request->validate(
            [
                'title' => 'required|max:45',
            ]
        );

        $isEnabled = 0;
        if (in_array($request->get('is_enabled'), [1, true, 'on'])) {
            $isEnabled = 1;
        }

        $data = [
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'is_enabled' => $isEnabled,
        ];
        $parentId = (int) $request->get('parent_id');
        if (!empty($parentId)) {
            $data['parent_id'] = $parentId;
        }

        $group = new Group($data);
        $group->save();

        return redirect(route('hhquiz.groups.index'))->with('success', 'Contact saved!');
    }

    public function getOne($id, Request $request)
    {
        $group = Group::currentUser()->whereId($id)->with('author')->first();
        if ($group == null) {
            abort(404);
        }

        return view('hhquiz::groups.show', compact('group'));
    }

    public function updateForm($id)
    {
        $group = Group::currentUser()->whereId($id)->first();
        if ($group == null) {
            abort(404);
        }
        $parents = Group::currentUser()->with('author')->where('id', '!=', $id)->get();

        return view('hhquiz::groups.update', compact('group', 'parents'));
    }

    public function update($id, Request $request)
    {
        $group = Group::currentUser()->whereId($id)->first();
        if ($group == null) {
            abort(404);
        }

        $request->validate(
            [
                'title' => 'required|max:45',
            ]
        );

        $group->title = $request->get('title');

        $isEnabled = 0;
        if (in_array($request->get('is_enabled'), [1, true, 'on'])) {
            $isEnabled = 1;
        }

        $group->is_enabled = $isEnabled;
        $group->description = $request->get('description');
        $parentId = (int) $request->get('parent_id');
        if (!empty($parentId)) {
            $group->parent_id = $parentId;
        } else {
            $group->parent_id = null;
        }
        $group->save();

        return redirect(route('hhquiz.groups.index'))->with('success', 'Contact saved!');
    }
}
