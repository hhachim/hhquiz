<?php

namespace Hachim\HHQuiz;

use Illuminate\Support\ServiceProvider;
use Hachim\HHQuiz\Components\AlertComponent;
use Illuminate\Support\Facades\Blade;

class HHQuizServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '../../routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/../migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'hhquiz');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../resources/views/' => resource_path('views'),
            ], ['hhquiz-views', 'hhquiz']);
        }

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../publishable/app/' => app_path(),
            ], ['hhquiz-views', 'hhquiz']);
        }

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../publishable/config/' => config_path(),
            ], ['hhquiz-config', 'hhquiz']);
        }

        Blade::component('package-alert', AlertComponent::class);
    }

    public function register()
    {
        /*$path = config_path('hhquiz.adminlte.override.php');
        if (is_file($path)) {
            $this->mergeConfigMuldimensional(
                $path,
                'hhquiz.adminlte.override',
                'adminlte'
            );
        }

        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'hhquiz');
        */
    }

    private function mergeConfigMuldimensional($configPath, $sourceKey, $destinationKey)
    {
        $this->mergeConfigFrom($configPath, $sourceKey);
        $sourceConfig = config($sourceKey);
        $destConfig = config($destinationKey);
        config([$destinationKey => array_replace_recursive($destConfig, $sourceConfig)]);
        unset($result);
    }
}
