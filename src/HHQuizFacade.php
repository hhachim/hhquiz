<?php

namespace Hachim\HHQuiz;


use Illuminate\Support\Facades\Facade;

/**
 * @see \Spatie\Skeleton\Skeleton
 */
class HHQuizFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'hhquiz';
    }
}
