<?php

//see example on TCG\Voyager\Models\Post

namespace Hachim\HHQuiz\Models\User;

use Hachim\HHQuiz\Models\BaseModel;

class Group extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hhquiz_user_group';

    public function parent()
    {
        return $this->belongsTo(self::class);
    }
}
