<?php
//see example on TCG\Voyager\Models\Post
namespace Hachim\HHQuiz\Models\User;

use Hachim\HHQuiz\Models\BaseModel;

class GroupSubscription extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hhquiz_user_group_subscription';
}