<?php

//see example on TCG\Voyager\Models\Post

namespace Hachim\HHQuiz\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use App\User;
use Webpatser\Uuid\Uuid;

class BaseModel extends EloquentModel
{
    use SoftDeletes;
    protected $guarded = ['id', 'uuid'];

    public function scopeCurrentUser($query)
    {
        return $query;
        //custome wereRaw scope example
        //return $query->whereRaw("id in (2,3,4) ");'Hachim\HHQuiz\Models\User\User'
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id', 'id');
    }

    public function save(array $options = [])
    {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$this->author_id && Auth::user()) {
            $this->author_id = Auth::user()->getKey();
        }

        if (isset($this->validated_at) && empty($this->validated_at) && !empty($this->is_validated)) {
            $this->validated_at = date('Y-m-d H:i:s');
            if (Auth::user()) {
                $this->validated_by = Auth::user()->getKey();
            }
        }

        if (isset($this->is_validated) && empty($this->is_validated)) {
            $this->validated_at = null;
        }

        if (isset($this->content)) {
            $this->content = html_entity_decode($this->content);
        }

        return parent::save($options);
    }

    /**
     *  Setup model event hooks.
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            if (empty($model->uuid)) {
                $model->uuid = (string) Uuid::generate(4); //ce-ci rend le uuid obligatoire dans chaque table hhquiz
            }
        });
    }
}
