<?php

//see example on TCG\Voyager\Models\Post

namespace Hachim\HHQuiz\Models\Question;

use Hachim\HHQuiz\Models\BaseModel;

class Question extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hhquiz_question';

    public function group()
    {
        return $this->belongsTo(
            'Hachim\HHQuiz\Models\Question\Group',
            'group_id', 'id'
        );
    }

    /**
     * Get the comments for the blog post.
     */
    public function choices()
    {
        return $this->hasMany('Hachim\HHQuiz\Models\Question\Choice', 'question_id', 'id');
    }

    public function quizzes()
    {
        return $this->belongsToMany(
            'Hachim\HHQuiz\Models\Quiz\Quiz',
            'hhquiz_quiz_question',
            'question_id',
            'quiz_id'
        )->with('author');
    }

    public function scopeParentGroup($query, $parentGroupId)
    {
        $cteQuery = '
        WITH RECURSIVE group_children as ( 
            SELECT id as child FROM hhquiz_question_group WHERE id='.(int) $parentGroupId.'
          UNION
            SELECT hhquiz_question_group.id FROM hhquiz_question_group, group_children WHERE group_children.child= hhquiz_question_group.parent_id 
        ) 
        SELECT * FROM group_children
        ';
        //$groups = \DB::select($sqlQuery, array(Auth::user()->getKey()));

        return $query->whereRaw("group_id in ( $cteQuery )");
        //custome wereRaw scope example
        //return $query->whereRaw("id in (2,3,4) ");'Hachim\HHQuiz\Models\User\User'
    }
}
