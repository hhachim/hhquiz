<?php

//see example on TCG\Voyager\Models\Post

namespace Hachim\HHQuiz\Models\Question;

use Hachim\HHQuiz\Models\BaseModel;

class Choice extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hhquiz_question_choice';

    public function question()
    {
        return $this->belongsTo(
            'Hachim\HHQuiz\Models\Question\Question',
            'question_id', 'id'
        );
    }
}
