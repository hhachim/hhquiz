<?php
//see example on TCG\Voyager\Models\Post
namespace Hachim\HHQuiz\Models\Quiz;

use Hachim\HHQuiz\Models\BaseModel;

class User extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hhquiz_quiz_user';

    protected $guarded = ['id', 'uuid'];

   
    public function user()
    {
        return $this->belongsTo('Hachim\HHQuiz\Models\User\User','user_id','id');
    }

    public function group()
    {
        return $this->belongsTo('Hachim\HHQuiz\Models\User\Group', 'group_id','id');
    }

    public function quiz()
    {
        return $this->belongsTo('Hachim\HHQuiz\Models\Quiz\Quiz', 'quiz_id','id');
    }
}