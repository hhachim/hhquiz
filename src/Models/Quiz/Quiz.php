<?php

namespace Hachim\HHQuiz\Models\Quiz;

use Hachim\HHQuiz\Models\BaseModel;

class Quiz extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hhquiz_quiz';

    public function requiredQuiz()
    {
        return $this->belongsTo(self::class, 'required_quiz_id', 'id');
    }

    public function questions()
    {
        return $this->belongsToMany(
            'Hachim\HHQuiz\Models\Question\Question',
            'hhquiz_quiz_question',
            'quiz_id',
            'question_id'
        )->with('choices');
    }
}
