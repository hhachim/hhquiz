<?php

//see example on TCG\Voyager\Models\Post

namespace Hachim\HHQuiz\Models\Quiz;

use Hachim\HHQuiz\Models\BaseModel;

class Group extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hhquiz_quiz_group';

    public function parent()
    {
        return $this->belongsTo(self::class);
    }

    public function scopeLeaves($query)
    {
        return $query->whereRaw(
            'id in (SELECT t.id 
                    FROM hhquiz_question_group t 
                    WHERE NOT EXISTS (SELECT 1 FROM hhquiz_question_group where parent_id = t.id)
                   )'
        );
        //custome wereRaw scope example
        //return $query->whereRaw("id in (2,3,4) ");'Hachim\HHQuiz\Models\User\User'
    }

    public function scopeChildrenOf($query, $parentGroupId)
    {
        $parentGroupId = (int) $parentGroupId;
        $cteQuery = '
        WITH RECURSIVE group_children as ( 
            SELECT id as child FROM hhquiz_question_group WHERE id=' . $parentGroupId . '
          UNION
            SELECT hhquiz_question_group.id FROM hhquiz_question_group, group_children WHERE group_children.child= hhquiz_question_group.parent_id 
        ) 
        SELECT * FROM group_children
        ';
        //$groups = \DB::select($sqlQuery, array(Auth::user()->getKey()));

        return $query->whereRaw("id in ( $cteQuery ) AND id != $parentGroupId ");
        //custome wereRaw scope example
        //return $query->whereRaw("id in (2,3,4) ");'Hachim\HHQuiz\Models\User\User'
    }

    public function scopeParentsOf($query, $parentGroupId)
    {
        $parentGroupId = (int) $parentGroupId;
        $cteQuery = '
        WITH RECURSIVE group_parents as ( 
            SELECT parent_id as parent FROM hhquiz_question_group WHERE id=' . $parentGroupId . ' 
          UNION
            SELECT hhquiz_question_group.parent_id 
            FROM hhquiz_question_group, group_parents 
            WHERE group_parents.parent= hhquiz_question_group.id  AND hhquiz_question_group.parent_id is not null
        ) 
        SELECT * FROM group_parents
        ';
        //$groups = \DB::select($sqlQuery, array(Auth::user()->getKey()));

        return $query->whereRaw("id in ( $cteQuery ) AND id != $parentGroupId ");
        //custome wereRaw scope example
        //return $query->whereRaw("id in (2,3,4) ");'Hachim\HHQuiz\Models\User\User'
    }
}
