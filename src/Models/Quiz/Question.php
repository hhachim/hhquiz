<?php

namespace Hachim\HHQuiz\Models\Quiz;

use Hachim\HHQuiz\Models\BaseModel;

class Question extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hhquiz_quiz_question';
}
