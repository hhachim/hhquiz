<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateQuizMigration extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $sql = "CREATE TABLE `hhquiz_quiz` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
            `uuid` varchar(36) NOT NULL,
            `description` text COLLATE utf8mb4_unicode_ci,
            `is_enabled` tinyint(1) DEFAULT '0',
            `select_user_failed_questions_first` tinyint(1) DEFAULT '0',
            `required_quiz_id` int(11) DEFAULT NULL,
            `required_quiz_percent_note` float(4) DEFAULT NULL,
            `order` int(11) NULL DEFAULT '0',
            `author_id` bigint(20) unsigned NOT NULL,
            `created_at` datetime NOT NULL,
            `updated_at` datetime NOT NULL,
            `deleted_at` datetime DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_quiz_1_idx` (`required_quiz_id`),
            KEY `fk_quiz_2_idx` (`author_id`),
            CONSTRAINT `fk_quiz_1` FOREIGN KEY (`required_quiz_id`) REFERENCES `hhquiz_quiz` (`id`),
            CONSTRAINT `fk_quiz_2` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('hhquiz_quiz');
    }
}
