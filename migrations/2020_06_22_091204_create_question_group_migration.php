<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateQuestionGroupMigration extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $sql = "CREATE TABLE `hhquiz_question_group` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `uuid` varchar(36) NOT NULL,
            `title` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
            `description` text COLLATE utf8mb4_unicode_ci,
            `is_enabled` tinyint(1) DEFAULT '0',
            `parent_id` int(11) DEFAULT NULL,
            `order` int(11) NULL DEFAULT '0',
            `author_id` bigint(20) unsigned NOT NULL,
            `created_at` datetime NOT NULL,
            `updated_at` datetime NOT NULL,
            `deleted_at` datetime DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_topic_group_1_idx` (`parent_id`),
            KEY `fk_topic_group_2_idx` (`author_id`),
            CONSTRAINT `fk_topic_group_1` FOREIGN KEY (`parent_id`) REFERENCES `hhquiz_question_group` (`id`),
            CONSTRAINT `fk_topic_group_2` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='peut etre un chapitre, un sous chapitre..'";

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('hhquiz_question_group');
    }
}
