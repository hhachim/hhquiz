<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateQuestionMigration extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $sql = "CREATE TABLE `hhquiz_question` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
            `uuid` varchar(36) NOT NULL,
            `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
            `keywords` text COLLATE utf8mb4_unicode_ci,
            `feedback` text COLLATE utf8mb4_unicode_ci,
            `is_enabled` tinyint(1) DEFAULT '0',
            `is_validated` tinyint(1) DEFAULT '0',
            `answer_type` enum('multichoices','shortanswers') NOT NULL,
            `content_type` enum('html','image','url','video','youtube-vdeo','other') default 'html',
            `order` int(11) NULL DEFAULT '0',
            `author_id` bigint(20) unsigned NOT NULL,
            `validated_by` bigint(20) unsigned DEFAULT NULL,
            `group_id` int(11) NULL,
            `created_at` datetime NOT NULL,
            `updated_at` datetime NOT NULL,
            `deleted_at` datetime DEFAULT NULL,
            `validated_at` datetime DEFAULT NULL,
            PRIMARY KEY (`id`),
            FULLTEXT INDEX `ft_question_1_idx` (`keywords` ASC),
            FULLTEXT INDEX `ft_question_3_idx` (`content` ASC),
            KEY `fk_question_1_idx` (`group_id`),
            KEY `fk_question_2_idx` (`author_id`),
            KEY `fk_question_3_idx` (`validated_by`),
            CONSTRAINT `fk_question_1` FOREIGN KEY (`group_id`) REFERENCES `hhquiz_question_group` (`id`),
            CONSTRAINT `fk_question_2` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
            CONSTRAINT `fk_question_3` FOREIGN KEY (`validated_by`) REFERENCES `users` (`id`)
           ) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci";

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('hhquiz_question');
    }
}
