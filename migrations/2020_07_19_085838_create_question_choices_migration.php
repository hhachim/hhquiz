<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateQuestionChoicesMigration extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        /*
        - question_id
- content
- is_good_answer
- feedback_wrong_answer
- feedback_good_answer
*/
        $sql = 'CREATE TABLE `hhquiz_question_choice` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `uuid` varchar(36) NOT NULL,
            `content` varchar(255) NOT NULL,
            `feedback` varchar(255) NULL,
            `is_good_answer` tinyint(1) NOT NULL,
            `author_id` bigint(20) unsigned NOT NULL,
            `question_id` int(11) NULL,
            `created_at` datetime NOT NULL,
            `updated_at` datetime NOT NULL,
            `deleted_at` datetime DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `ft_question_choices_1_idx` (`question_id`),
            KEY `ft_question_choices_2_idx` (`author_id`),
            CONSTRAINT `fk_question_choices_1` FOREIGN KEY (`question_id`) REFERENCES `hhquiz_question` (`id`),
            CONSTRAINT `fk_question_choices_2` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`)
           ) ENGINE=InnoDB DEFAULT COLLATE=utf8mb4_unicode_ci';

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('hhquiz_question_choice');
    }
}
