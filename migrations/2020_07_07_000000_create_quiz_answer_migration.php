<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateQuizAnswerMigration extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $sql = "CREATE TABLE `hhquiz_quiz_answer` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `external_username` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `uuid` varchar(36) NOT NULL,
            `is_expected_answer` tinyint(1) DEFAULT NULL,
            `submited_answer` text COLLATE utf8mb4_unicode_ci,
            `expected_answer` text COLLATE utf8mb4_unicode_ci,
            `useragent` text COLLATE utf8mb4_unicode_ci,
            `ip` varchar(20) DEFAULT NULL,
            `quiz_id` int(11) NOT NULL,
            `question_group_id` int(11) DEFAULT NULL,
            `question_id` int(11) NOT NULL,
            `order` int(11) NULL DEFAULT '0',
            `author_id` bigint(20) unsigned NOT NULL,
            `created_at` datetime NOT NULL,
            `updated_at` datetime NOT NULL,
            `deleted_at` datetime DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_quiz_answer_1_idx` (`quiz_id`),
            KEY `fk_quiz_answer_2_idx` (`author_id`),
            KEY `fk_quiz_answer_3_idx` (`question_group_id`),
            KEY `fk_quiz_answer_4_idx` (`question_id`),
            CONSTRAINT `fk_quiz_answer_1_idx` FOREIGN KEY (`quiz_id`) REFERENCES `hhquiz_quiz_question` (`quiz_id`),
            CONSTRAINT `fk_quiz_answer_2_idx` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
            CONSTRAINT `fk_quiz_answer_3_idx` FOREIGN KEY (`question_group_id`) REFERENCES `hhquiz_quiz_question_from_group`(`question_group_id`),
            CONSTRAINT `fk_quiz_answer_4_idx` FOREIGN KEY (`question_id`) REFERENCES `hhquiz_question`(`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('hhquiz_quiz_answer');
    }
}
