<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGroupSubscriptionMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $sql = "CREATE TABLE `hhquiz_user_group_subscription` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `uuid` varchar(36) NOT NULL,
            `is_enabled` tinyint(1) DEFAULT '1',
            `start_at` datetime DEFAULT NULL,
            `end_at` datetime DEFAULT NULL,
            `group_id` int(11) DEFAULT NULL,
            `user_id` bigint(20) unsigned DEFAULT NULL,
            `author_id` bigint(20) unsigned NOT NULL,
            `order` int(11) NULL DEFAULT '0',            
            `created_at` datetime NOT NULL,
            `updated_at` datetime NOT NULL,
            `deleted_at` datetime DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_user_group_subscription_1_idx` (`group_id`),
            KEY `fk_user_group_subscription_2_idx` (`user_id`),
            KEY `fk_user_group_subscription_3_idx` (`author_id`),
            CONSTRAINT `fk_user_group_subscription_1_idx` FOREIGN KEY (`group_id`) REFERENCES `hhquiz_user_group`(`id`),
            CONSTRAINT `fk_user_group_subscription_2_idx` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
            CONSTRAINT `fk_user_group_subscription_3_idx` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hhquiz_user_group_subscription');
    }
}
