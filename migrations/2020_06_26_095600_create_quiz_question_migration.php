<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateQuizQuestionMigration extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $sql = "CREATE TABLE `hhquiz_quiz_question` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `uuid` varchar(36) NOT NULL,
            `quiz_id` int(11) NOT NULL,
            `question_id` int(11) NOT NULL,
            `order` int(11) NULL DEFAULT '0',
            `author_id` bigint(20) unsigned NOT NULL,
            `created_at` datetime NOT NULL,
            `updated_at` datetime NOT NULL,
            `deleted_at` datetime DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_quiz_question_1_idx` (`quiz_id`),
            KEY `fk_quiz_question_2_idx` (`question_id`),
            KEY `fk_quiz_question_3_idx` (`author_id`),
            CONSTRAINT `fk_quiz_question_1` FOREIGN KEY (`quiz_id`) REFERENCES `hhquiz_quiz` (`id`),
            CONSTRAINT `fk_quiz_question_2` FOREIGN KEY (`question_id`) REFERENCES `hhquiz_question` (`id`),
            CONSTRAINT `fk_quiz_question_3` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
            CONSTRAINT `quiz_question_uniq_1` UNIQUE (quiz_id,question_id)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('hhquiz_quiz_question');
    }
}
