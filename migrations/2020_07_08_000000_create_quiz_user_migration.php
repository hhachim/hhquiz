<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizUserMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $sql = "CREATE TABLE `hhquiz_quiz_user` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `uuid` varchar(36) NOT NULL,
            `is_enabled` tinyint(1) DEFAULT '1',
            `quiz_id` int(11) NOT NULL,
            `must_start_after` datetime DEFAULT NULL,
            `must_end_before` datetime DEFAULT NULL,
            `started_at` datetime DEFAULT NULL,
            `ended_at` datetime DEFAULT NULL,
            `group_id` int(11) DEFAULT NULL,
            `max_attemps` int(11) DEFAULT '10',
            `user_id` bigint(20) unsigned DEFAULT NULL,
            `author_id` bigint(20) unsigned NOT NULL,
            `order` int(11) NULL DEFAULT '0',            
            `created_at` datetime NOT NULL,
            `updated_at` datetime NOT NULL,
            `deleted_at` datetime DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_quiz_user_1_idx` (`quiz_id`),
            KEY `fk_quiz_user_2_idx` (`group_id`),
            KEY `fk_quiz_user_3_idx` (`user_id`),
            KEY `fk_quiz_user_4_idx` (`author_id`),
            CONSTRAINT `fk_quiz_user_1_idx` FOREIGN KEY (`quiz_id`) REFERENCES `hhquiz_quiz` (`id`),
            CONSTRAINT `fk_quiz_user_2_idx` FOREIGN KEY (`group_id`) REFERENCES `hhquiz_user_group`(`id`),
            CONSTRAINT `fk_quiz_user_3_idx` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
            CONSTRAINT `fk_quiz_user_4_idx` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hhquiz_quiz_user');
    }
}
