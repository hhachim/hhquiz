<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateQuizQuestionFromGroupMigration extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $sql = "CREATE TABLE `hhquiz_quiz_question_from_group` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `uuid` varchar(36) NOT NULL,
            `quiz_id` int(11) NOT NULL,
            `question_group_id` int(11) NOT NULL,
            `nb_questions` int (11) NOT NULL,
            `use_random_order` tinyint(1) NULL DEFAULT '0',
            `order` int(11) NULL DEFAULT '0',
            `author_id` bigint(20) unsigned NOT NULL,
            `created_at` datetime NOT NULL,
            `updated_at` datetime NOT NULL,
            `deleted_at` datetime DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `fk_quiz_question_from_group_1_idx` (`quiz_id`),
            KEY `fk_quiz_question_from_group_2_idx` (`question_group_id`),
            KEY `fk_quiz_question_from_group_3_idx` (`author_id`),
            CONSTRAINT `fk_quiz_question_from_group_1` FOREIGN KEY (`quiz_id`) REFERENCES `hhquiz_quiz` (`id`),
            CONSTRAINT `fk_quiz_question_from_group_2` FOREIGN KEY (`question_group_id`) REFERENCES `hhquiz_question_group` (`id`),
            CONSTRAINT `fk_quiz_question_from_group_3` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
            CONSTRAINT `quiz_question_from_group_uniq_1` UNIQUE (quiz_id,question_group_id)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('hhquiz_quiz_question_from_group');
    }
}
