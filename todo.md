# V1
[] hhquiz setting seeder (ex admin site_name)
[] seeder for student role and student_user
[] dans la table question, ajouter une colonne short_answer1 varchar(100), short_answer2, short_answer3, short_answer4???
[] dans les traductions, remplacer 'groupe' par 'classe' pour les groupes d'utilisateur
[] Gestion/interception des erreurs mysql : ne pas les afficher à l'écran si l'utilisateur n'est pas l'admin
[] dashboard widgets en fonction du role et de l'utilisateur connecté

# V1 - FRONT
[] dashboard widgets for student use
    - My Quizzes
    - Continue my last quiz
    - unterminated quizzes
    - My highest score
    - My avg score
    - All my scores
    - Progressions
    - ...

# V2
[] notion de context dans l'url
[] dans le userScope, prendre en compte le contexte et les autres paramètres de l'url
[] package hhachim/hhquiz-import-export
[] étude différence entre private, protected, public

# File rouge
[] cheick homogenisation bdd & divers (taille de champs):
    - commentaire
    - taille
    - nommenclature
    - encodage
    - index uniq
    - indexes aux bons endroit
    - fulltext search
    - valeurs par défaut
    - valeurs obligatoires
    - suppression champs residuels/inutiles issue des copier/coller
    - vérifier qu'il y ait le bon nom de table dans la fonction down()
    - css champs requis (asterix ou border_color)
[] ajout traductions manquantes
[] tests QA :
    - essayer de provoquer des erreurs mysql
    - analyser chaque page bread
    - analyser chaque page read
    - analyser chaque page edit
    - analyser chaque page delete
    - traductions
    - order
    - colonne nécessaire sur le bread de chaque table

# Nice to have
[] .form-group-required dans le fichier custom hhquiz.css
