@extends('hhquiz::layouts.questions.choices.form')
@php
  $title 		=  'Ajout d\'un choix à la question';
  $url = route('hhquiz.questions.choices.create.post',$question->uuid);
  $methode = 'POST';
@endphp