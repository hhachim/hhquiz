@extends('hhquiz::layouts.questions.choices.form')
@php
  $title 		=  'Mise à jour d\'un choix multiple';
  $url = route('hhquiz.questions.choices.update.put', [$question->uuid, $choice->uuid]);
  $methode = 'PUT';
@endphp