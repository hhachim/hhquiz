@extends('adminlte::page')

@section('title', 'Les Quiz')

@section('content_header')
<h1>Classeur :
    @isset($group->parent->parent->parent->title)
    <small><a href="{{route('hhquiz.questions.groups.get.one',$group->parent->parent->parent->id)}}"">{{ $group->parent->parent->parent->title }} /</a></small>
    @endisset
    @isset($group->parent->parent->title)
    <small><a href="{{route('hhquiz.questions.groups.get.one',$group->parent->parent->id)}}"">{{ $group->parent->parent->title }} /</a></small>
    @endisset 
    @isset($group->parent->title)
    <small><a href="{{route('hhquiz.questions.groups.get.one',$group->parent->id)}}"">{{ $group->parent->title }} /</a></small>
    @endisset
    {{ $group->title }} 
</h1>
@stop

    @section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-info"></i>
                        Information
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <dl>
                        <dt>{{ $group->description }}</dt>
                    </dl>
                    <table class="table table-sm">
                        <tbody>
                            <tr>
                                <td>Id :</td>
                                <td>{{ $group->id }}</td>
                            <tr>
                                <td>Créé le :</td>
                                <td>{{ $group->created_at }}</td>
                            <tr>
                                <td>Mis à jour le :</td>
                                <td>{{ $group->updated_at }}</td>
                            </tr>
                            <tr>
                                <td>Créé par :</td>
                                <td>{{ $group->author->name }}</td>
                            </tr>
                            <tr>
                        </tbody>
                    </table>
                    <a class="btn btn-info btn-sm" href="{{ route('hhquiz.questions.groups.update.form', $group->uuid) }}">
                        <i class="fas fa-edit">
                        </i>
                        Modifier
                      </a>
                      <form action="#" method="POST" onsubmit="return confirm('Message confirmation');" style="display: inline-block;">
                          <input type="hidden" name="_method" value="DELETE">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"> Supprimer</i></button>
                      </form>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- ./col -->
    </div>

    @stop