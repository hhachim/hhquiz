@extends('hhquiz::layouts.questions.form')
@php
  $title 		=  'Mise à jour d`une question';
  $url = route('hhquiz.questions.update.put', ['uuid' => request('uuid')]);
  $methode = 'PUT';
@endphp