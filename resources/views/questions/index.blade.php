@extends('adminlte::page')

@section('title', 'Les Questions')

@section('content_header')
<h1>Les Questions
    @if(!empty($group)) 
        du groupe : 
        @isset($group->parent->parent->parent->title)
        <small><a href="{{route('hhquiz.questions.groups.get.one',$group->parent->parent->parent->id)}}"">{{ $group->parent->parent->parent->title }} /</a></small>
        @endisset
        @isset($group->parent->parent->title)
        <small><a href="{{route('hhquiz.questions.groups.get.one',$group->parent->parent->id)}}"">{{ $group->parent->parent->title }} /</a></small>
        @endisset 
        @isset($group->parent->title)
        <small><a href="{{route('hhquiz.questions.groups.get.one',$group->parent->id)}}"">{{ $group->parent->title }} /</a></small>
        @endisset
        <small><a href="{{route('hhquiz.questions.groups.get.one',$group->id)}}"">{{ $group->title }}</a></small>
    @endif
</h1>
@stop

@section('content')


<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Filtres pour mieux cibler les questions souhaitées</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <!-- /.card-header -->

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <div class="card-body">
                <!-- form start -->
            
                <form role="form" id="quickForm" novalidate="novalidate" method="GET"
            action="{{ route('hhquiz.questions.index')}}">
                    <div class="row">
                        <div class="col">                           
                            <div class="form-group row">
                                <label for="question_group_typeahead" class="col-sm-2 col-form-label">Classeur</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="question_group_typeahead"
                                        placeholder="Commencer à tapper votre classeur ici..."
                                        @if(!empty($groupPath))
                                        value="{{ $groupPath }}"
                                        @else 
                                            value="Tous"
                                        @endif
                                        autocomplete="off">
                                    <input type='hidden' id="question_group"
                                        @if(!empty($group))
                                        value="{{ $group->id }}"
                                        @endif
                                    name="question_group">
                                    <small class="form-text text-muted">Rechercher par un ou plusieurs mots clés</small>
                                </div>
                            </div>                            
                            <div class="form-group row">
                                <label for="keywords" class="col-sm-2 col-form-label">Auteur</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="author_id" name="author_id">
                                    <option value='{{ $me->getKey() }}'
                                        @if (isset($authorId) && $authorId == $me->getKey())
                                            selected                                            
                                        @endif
                                        >Moi</option>
                                        @foreach ($authors as $author)
                                    <option value="{{ $author->id }}"
                                        @if (isset($authorId) && $authorId == $author->id)
                                            selected                                            
                                        @endif
                                        >{{ $author->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <select class="form-control" required id="answer_type" name="answer_type">
                                    <option value='0'
                                    @empty($answerType)
                                        selected
                                    @endempty
                                    >--Type de questions--</option>
                                    <option value='multichoices'
                                    @if (isset($answerType) && $answerType == 'multichoices')
                                        selected                                            
                                    @endif
                                    >Questions type "Choix multiples ou vrai faux"</option>
                                    <option value='shortanswers'
                                    @if (isset($answerType) && $answerType == 'shortanswers')
                                        selected                                            
                                    @endif
                                    >Questions à "Réponses courtes"</option>
                                </select>
                            </div>
                            <!--
                            <div class="form-group">
                                <label>Période de mise à jour</label>

                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control float-right" id="update_between"
                                        name="update_between">
                                </div>
                            </div>
                            -->
                            <button type="submit" class="btn btn-primary">Filter</button>
                            <small class="form-text text-muted">Seules les questions <b>actives</b> seront affichées!</small>
                        </div>
                    </div><!-- from row end-->
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
    <div class="col-12">
        <div class="card">
            
            <div class="card-header">
                <a href="{{ route('hhquiz.questions.create.form') }}" class="btn btn-success">Créer une question</a>
                <div class="card-tools"> 
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-hover datatable">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Status</th>
                            <th>Type</th>
                            <th>Classeur</th>
                            <th>Créé par</th>
                            <th>Créé le</th>
                            <th>Mise à jour le</th>
                            <th>ID</th>
                            <th width="250">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($questions as $question)
                    <tr>
                        <td><a href="{{ route('hhquiz.questions.get.one', $question->uuid) }}">{{$question->title}}</a></td>
                        <td>
                            @if ($question->is_enabled == 1)
                                <span class="badge badge-success">Actif</span>
                            @else 
                                <span class="badge badge-danger">Inactif</span>
                            @endif
                        </td>
                        <td>
                            @if ($question->answer_type == 'multichoices')
                                <span class="badge badge-warning">Choix multiples</span>
                            @else 
                                <span class="badge badge-secondary">Réponses courtes</span>
                            @endif
                        </td>
                        <td>
                            @empty($question->group_id)
                            Non classé
                            @endempty
                            @isset($question->group->parent->parent->parent->title)
                            <small><a href="{{route('hhquiz.questions.groups.get.one',$question->group->parent->parent->parent->uuid)}}"">{{ $question->group->parent->parent->parent->title }} /</a></small>
                            @endisset
                            @isset($question->group->parent->parent->title)
                            <small><a href="{{route('hhquiz.questions.groups.get.one',$question->group->parent->parent->uuid)}}"">{{ $question->group->parent->parent->title }} /</a></small>
                            @endisset 
                            @isset($question->group->parent->title)
                            <small><a href="{{route('hhquiz.questions.groups.get.one',$question->group->parent->uuid)}}"">{{ $question->group->parent->title }} /</a></small>
                            @endisset
                            @isset($question->group->title)
                            <a href="{{route('hhquiz.questions.groups.get.one',$question->group->uuid)}}"">{{ $question->group->title }}</a>
                            @endisset
                        </td>
                        <td>{{$question->author->name}}</td>
                        <td>{{$question->created_at}}</td>
                        <td>{{$question->updated_at}}</td>
                        <td>{{$question->id}}</td>
                        <td class="project-actions text-right">
                            <a class="btn btn-primary btn-sm" href="{{ route('hhquiz.questions.get.one', $question->uuid) }}">
                              <i class="fas fa-eye">
                              </i>
                            </a>
                            <a class="btn btn-info btn-sm" href="{{ route('hhquiz.questions.update.form', $question->uuid) }}">
                              <i class="fas fa-edit">
                              </i>
                            </a>
                            <!--
                            <form action="#" method="POST" onsubmit="return confirm('Message confirmation');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            </form>
                        -->
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $questions->withQueryString()->links() }}
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@stop

@section('css')
<!--
<link rel="stylesheet" href="/css/admin_custom.css">
-->
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/datatables-plugins/responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/summernote/summernote-bs4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('css/typeaheadjs.css') }}">

@stop

@section('js')
<script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/bootstrap/typeahead.bundle.js') }}"></script>
<script src="{{ asset('js/bootstrap/handlebars.js') }}"></script>

<script id="empty-template" type="text/x-handlebars-template">
    <div class="EmptyMessage">Votre recherche ne donne aucun résultat!<p>un autre paragraphe</p></div>
</script>
<script id="result-template" type="text/x-handlebars-template">
    <div class="ProfileCard u-cf">
      <!--
        <img class="ProfileCard-avatar" src="https://picsum.photos/50/50">
      -->
      <div class="ProfileCard-details">
        <div class="ProfileCard-realName">@{{id}}# - @{{path}}</div>
      </div>

    </div>
</script>
<script>
    $(function() {
        var table = $('.datatable').DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "ordering": true,
            "info": false,
            "autoWidth": true,
            "responsive": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json"
            }
        });

        empty = Handlebars.compile($("#empty-template").html());
        template = Handlebars.compile($("#result-template").html());

        var questionGroupSelectedItem = @json($group);
        console.log("hachim initial group:", questionGroupSelectedItem);

        //https://github.com/twitter/typeahead.js/blob/master/doc/jquery_typeahead.md#options
        // et https://twitter.github.io/typeahead.js/ pour les templates empty + result
        var typeAhead = $('#question_group_typeahead').typeahead(
            {
                hint: true,
                highlight: true,
                minLength: 1,
            },
            {            
                name: 'title',
                async: true,
                limit: 30,
                displayKey: 'id',
                templates: {
                    suggestion: template,
                    empty: empty
                },
                display: 'path',//url: "{{ route('hhquiz.ajax.json.questions.groups.search') }}",
                source: function (query, processSync, processAsync) {
                    processSync([{'id':0, 'path': 'Tous'}]);
                    return $.ajax({
                        url: "{{ route('hhquiz.ajax.json.questions.groups.search') }}", 
                        type: 'GET',
                        data: {query: query},
                        dataType: 'json',
                        success: function (json) {
                            console.log("hachim:",json);
                            return processAsync(json);
                        }
                    });
                }
        });

        typeAhead.bind('typeahead:select', function(ev, suggestion) {
            questionGroupSelectedItem = suggestion;
            $('#question_group').val(questionGroupSelectedItem.id);
        });

        typeAhead.bind('typeahead:close', function() {
            if(questionGroupSelectedItem && questionGroupSelectedItem.path !== undefined) {
                $('#question_group_typeahead').val( questionGroupSelectedItem.path);
            }
        });

    });
</script>
@stop

@push('js')
    <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-validation/additional-methods.min.js') }}"></script>
    <script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('vendor/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/daterangepicker/daterangepicker.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                //Date range picker
                $("#update_between,#create_between").each(function () {
                    $(this).daterangepicker({
                        "locale": {
                            "format": "DD/MM/YYYY",
                            "separator": " - ",
                            "applyLabel": "Valider",
                            "cancelLabel": "Annuler",
                            "fromLabel": "De",
                            "toLabel": "A",
                            "daysOfWeek": [
                                "Dim",
                                "Lun",
                                "Mar",
                                "Mer",
                                "Jeu",
                                "Ven",
                                "Sam"
                            ],
                            "monthNames": [
                                "Janv",
                                "Fév",
                                "Mar",
                                "Avr",
                                "Mai",
                                "Jun",
                                "Jui",
                                "Aou",
                                "Sept",
                                "Oct",
                                "Nov",
                                "Dec"
                            ],
                            "firstDay": 1
                        },
                        timePicker: false
                    });
                });
            });
            $('.textarea').summernote();
            jQuery.validator.setDefaults({
                // This will ignore all hidden elements alongside `contenteditable` elements
                // that have no `name` attribute
                //https://stackoverflow.com/questions/42086841/jquery-validate-with-summernote-editor-error-cannot-read-property-replace-of
                //https://github.com/jquery-validation/jquery-validation/issues/1875
                ignore: ":hidden, [contenteditable='true']:not([name])"
            });


            $('#quickForm').validate({
                /*rules: {
                    title: {
                        required: true,
                        minlength: 1
                    },
                },
                messages: {
                    title: {
                        required: "Veuillez rentrer le nom du quiz",
                        minlength: "Veuillez saisir au moins 4 caractères",
                    }
                },*/
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });

    </script>
@endpush