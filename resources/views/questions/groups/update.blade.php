@extends('hhquiz::layouts.questions.groups.form')
@php
  $title 		=  'Mise à jour d\'un classeur de sujets';
  $url = route('hhquiz.questions.groups.update.put', ['uuid' => request('uuid')]);
  $methode = 'PUT';
@endphp