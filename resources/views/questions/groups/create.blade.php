@extends('hhquiz::layouts.questions.groups.form')
@php
  $title 		=  'Création d\'un classeur de sujets';
  $url = route('hhquiz.questions.groups.create.post');
  $methode = 'POST';
@endphp