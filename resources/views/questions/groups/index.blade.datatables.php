@extends('adminlte::page')

@section('title', 'Classeurs de sujets')

@section('content_header')
<h1>Les classeurs de questions</h1>
@stop

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            
            <div class="card-header">
                <a href="{{ route('hhquiz.questions.groups.create.form') }}" class="btn btn-success">Créer un classeur</a>
                <div class="card-tools"> 
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered table-hover datatable">
                    <thead>
                        <tr>
                            <td>Nom</td>
                            <td>Status</td>
                            <td>Créé par</td>
                            <td>Créé le</td>
                            <td>Mise à jour le</td>
                            <td>ID</td>
                            <td width="70">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($groups as $group)
                    <tr>
                        <td>
                            @isset($group->parent->parent->title)
                            <small><a href="{{route('hhquiz.questions.groups.get.one',$group->parent->parent->uuid)}}"">{{ $group->parent->parent->title }} / </a></small>
                            @endisset
                            @isset($group->parent->title)
                        <small><a href="{{route('hhquiz.questions.groups.get.one',$group->parent->uuid)}}"">{{ $group->parent->title }} / </a> </small>
                            @endisset
                            <a href="{{route('hhquiz.questions.groups.get.one',$group->uuid)}}"">{{$group->title}}</a>
                        </td>
                        <td>
                            @if ($group->is_enabled == 1)
                                <span class="badge badge-success">Actif</span>
                            @else 
                                <span class="badge badge-danger">Inactif</span>
                            @endif
                        </td>
                        <td>{{$group->author->name}}</td>
                        <td>{{$group->created_at}}</td>
                        <td>{{$group->updated_at}}</td>
                        <td>{{$group->id}}</td>
                        <td class="project-actions text-right">
                            <a class="btn btn-primary btn-sm" href="{{ route('hhquiz.questions.groups.get.one', $group->uuid) }}">
                              <i class="fas fa-eye">
                              </i>
                            </a>
                            <a class="btn btn-info btn-sm" href="{{ route('hhquiz.questions.groups.update.form', $group->uuid) }}">
                              <i class="fas fa-edit">
                              </i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $groups->withQueryString()->links() }}
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@stop

@section('css')
<!--
<link rel="stylesheet" href="/css/admin_custom.css">
-->
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/datatables-plugins/responsive/css/responsive.bootstrap4.min.css') }}">
@stop

@section('js')
<script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    $(function() {
        $('.datatable').DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "ordering": true,
            "info": false,
            "autoWidth": true,
            "responsive": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json"
            }
        });
    });
</script>
@stop