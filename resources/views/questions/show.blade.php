@extends('adminlte::page')

@section('title', 'Les Quiz')

@section('content_header')
<h1>Question : {{ $question->title }} </h1>
@stop

@section('content')

    <div class="row ">
        <div class="col-md-12">
            <div class="card card-{{ config('hhquiz.theme.color.info') }}">
                <div class="card-header">
                    <h3 class="card-title">
                    <i class="fas fa-{{ config('hhquiz.theme.color.info') }}"></i>
                        Information
                    </h3>
                    <div class="card-tools">
                        <a class="btn btn-{{ config('hhquiz.theme.color.info') }} btn-sm" href="{{ route('hhquiz.questions.update.form', $question->uuid) }}">
                            <i class="fas fa-edit">
                            </i>
                        </a>
                        <form action="#" method="POST" onsubmit="return confirm('Message confirmation');" style="display: inline-block;">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-{{ config('hhquiz.theme.color.info') }} btn-sm"><i class="fas fa-trash"></i></button>
                        </form>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <dl>
                        <dt>{!! $question->content !!}</dt>
                    </dl>
                    <div class="row">
                        <div class="col-md-6">
                    <table class="table table-sm">
                        <tbody>
                            <tr>
                                <td>Id :</td>
                                <td>{{ $question->id }}</td>
                            </tr>
                            <tr>
                                <td>Etat :</td>
                                <td>
                                    @if ($question->is_enabled == 1)
                                        <span class="badge badge-success">Actif</span>
                                    @else 
                                        <span class="badge badge-danger">Inactif</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Classeur</td>
                                <td>
                                    @empty($question->group_id)
                                    Non classé
                                    @endempty
                                    @isset($question->group->parent->parent->parent->title)
                                    <small><a href="{{route('hhquiz.questions.groups.get.one',$question->group->parent->parent->parent->uuid)}}"">{{ $question->group->parent->parent->parent->title }} /</a></small>
                                    @endisset
                                    @isset($question->group->parent->parent->title)
                                    <small><a href="{{route('hhquiz.questions.groups.get.one',$question->group->parent->parent->uuid)}}"">{{ $question->group->parent->parent->title }} /</a></small>
                                    @endisset 
                                    @isset($question->group->parent->title)
                                    <small><a href="{{route('hhquiz.questions.groups.get.one',$question->group->parent->uuid)}}"">{{ $question->group->parent->title }} /</a></small>
                                    @endisset
                                    @isset($question->group->title)
                                    <a href="{{route('hhquiz.questions.groups.get.one',$question->group->uuid)}}"">{{ $question->group->title }}</a>
                                    @endisset
                                </td>
                            </tr>
                            <tr>
                                <td>Type :</td>
                                <td>
                                    @if ($question->answer_type == 'multichoices')
                                        Question à choix multiples
                                    @else 
                                        Question à réponses courtes
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Créé le :</td>
                                <td>{{ $question->created_at }}</td>
                            </tr>
                            <tr>
                                <td>Mis à jour le :</td>
                                <td>{{ $question->updated_at }}</td>
                            </tr>
                            <tr>
                                <td>Créé par :</td>
                                <td>{{ $question->author->name }}</td>
                            </tr>
                        </tbody>
                    </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-sm">
                                <tbody>
                                    <tr>
                                        <td>Validée le :</td>
                                        <td>{{ $question->validated_at }}</td>
                                    </tr>
                                    <tr>
                                        <td>Validée par :</td>
                                        <td>
                                            @if(!empty($question->validated_by))
                                                Utilisateur {{ $question->validated_by }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mots clés :</td>
                                        <td>
                                           {{ $question->keywords }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Référence unique :</td>
                                        <td>
                                            {{ $question->uuid }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Quiz associés :</td>
                                        <td>
                                            {{ count($question->quizzes) }} Quiz directs
                                            <a class="btn btn-{{ config('hhquiz.theme.color.create-sub')}} btn-sm" href="{{ route('hhquiz.questions.choices.create.form', $question->uuid) }}">
                                                <i class="fas fa-tasks">
                                                </i>
                                                Associer à un Quiz
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nombre de réponses reçues :</td>
                                        <td>
                                            0 , <a href=''>Voir les statistiques</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>                    
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- ./col -->

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-dark">
                <div class="card-body">
                    <div class="bd-example bd-example-tabs">
                        <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active show" id="answers-tab" data-toggle="tab" href="#answers" role="tab" aria-controls="answers" aria-selected="true">Réponses attendues</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="quizzes-tab" data-toggle="tab" href="#quizzes" role="tab" aria-controls="quizzes" aria-selected="false">Quiz directs associés</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">statistiques</a>
                          </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                          <div class="tab-pane fade active show" id="answers" role="tabpanel" aria-labelledby="answers-tab">
                            @if(!empty($question->choices))
                            <a class="btn btn-{{ config('hhquiz.theme.color.create-sub')}} btn-sm" href="{{ route('hhquiz.questions.choices.create.form', $question->uuid) }}">
                                <i class="fas fa-tasks">
                                </i>
                                Ajouter un choix
                            </a>
                                <table class="table table-sm table-hover datatable">
                                    <thead>
                                        <tr>
                                            <th>
                                                Choix possibles 
                                            </th>
                                            <th>
                                                Bonne réponse?
                                            </th>
                                            <th>
                                                Feedback
                                            </th>
                                            <th>
                                                Id
                                            </th>
                                            <th width="100">
                                                Actions
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($question->choices as $choice)
                                        <tr>
                                            <td>{{ $choice->content }}</td>
                                            <td>
                                                <h5>
                                                @if ($choice->is_good_answer == 1)
                                                    <span class="badge badge-success">Oui</span>
                                                @else 
                                                    <span class="badge badge-danger">Non</span>
                                                @endif
                                                </h5>
                                            </td>
                                            <td>{{ $choice->feedback }}</td>
                                            <td>{{ $choice->id }}</td>
                                            <td>
                                                <a class="btn btn-info btn-sm" href="{{ route('hhquiz.questions.choices.update.form', [$question->uuid, $choice->uuid]) }}">
                                                    <i class="fas fa-edit">
                                                    </i>
                                                </a>
                                                <a class="btn btn-danger btn-sm" href="">
                                                    <i class="fas fa-trash">
                                                    </i>
                                                </a>
                                            </td>
                                        </tr>    
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                          <div class="tab-pane fade" id="quizzes" role="tabpanel" aria-labelledby="quizzes-tab">
                            <a class="btn btn-{{ config('hhquiz.theme.color.create-sub')}} btn-sm" href="{{ route('hhquiz.questions.choices.create.form', $question->uuid) }}">
                                <i class="fas fa-tasks">
                                </i>
                                Associer à un Quiz
                            </a>
                           
                            <table class="table table-sm table-hover datatable">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Auteur</th>
                                        <th>Status</th>
                                        <th>Créé le</th>
                                        <th>Mise à jour le</th>
                                        <th>Id</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if (count($question->quizzes) > 0)
                                    @foreach($question->quizzes as $quiz)
                                        <tr>
                                        <td> <a href="{{ route('hhquiz.quizzes.get.one', $quiz->uuid)}}"> {{ $quiz->title}} </a> </td>
                                            <td> {{ $quiz->author->name}} </td>
                                            <td>
                                                @if ($quiz->is_enabled == 1)
                                                    <span class="badge badge-success">Actif</span>
                                                @else 
                                                    <span class="badge badge-danger">Inactif</span>
                                                @endif
                                            </td>
                                            <td> {{ $quiz->created_at}} </td>
                                            <td> {{ $quiz->updated_at}} </td>
                                            <td> {{ $quiz->id}} </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                          </div>
                          <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
                          </div>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
@stop
<!-- section content -->

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/datatables-plugins/responsive/css/responsive.bootstrap4.min.css') }}">
@stop

@section('js')
<script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    $(function() {
        $('.datatable').DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "ordering": true,
            "info": false,
            "autoWidth": false,
            "responsive": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json"
            }
        });
    });
</script>
@stop