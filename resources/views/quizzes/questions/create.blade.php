@extends('adminlte::page')

@section('title')
Ajout de questions dans le Quiz {{ $quiz->title }} (n° {{ $quiz->id }})
@stop

    @section('content_header')
    Ajout de questions dans le Quiz {{ $quiz->title }} (n° {{ $quiz->id }})
    @stop

        @section('content')
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- jquery validation -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Filtres pour mieux cibler les questions souhaitées</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <div class="card-body">
                        <span class="badge badge-warning mb-3">Seules les questions activées seront affichées</span>

                        <!-- form start -->
                        <form role="form" id="quickForm" novalidate="novalidate" method="POST"
                            action="{{ route('hhquiz.quizzes.questions.create.post', $quiz->id) }}">
                            <div class="row">
                                <div class="col">
                                    <input type="hidden" name="_method" value="POST">
                                    @csrf
                                    <div class="form-group">
                                        <select class="form-control" id="answer_type" name="answer_type">
                                            <option selected disabled>--Auteur--</option>
                                            <option value='order_by_1'>Moi</option>
                                            <option value='order_by_1'>Trier par daate création décroissante</option>
                                        </select>
                                    </div>
                                    @if(isset($groups) && count($groups) > 0)
                                        <div class="form-group">
                                            <select class="form-control" id="group_id" name="group_id">
                                                <option selected disabled>--Classeur--</option>
                                                @foreach($groups as $group)
                                                    <option value="{{ $group->id }}">
                                                        @isset($group->parent->parent->parent->title)
                                                            {{ $group->parent->parent->parent->title }}/
                                                        @endisset
                                                        @isset($group->parent->parent->title)
                                                            {{ $group->parent->parent->title }}/
                                                        @endisset
                                                        @isset($group->parent->title)
                                                            {{ $group->parent->title }}/
                                                        @endisset
                                                        @isset($group->title)
                                                            {{ $group->title }}
                                                        @endisset
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <select class="form-control" id="answer_type" name="answer_type">
                                            <option selected disabled>--Type de questions--</option>
                                            <option value='multichoices'>Choix multiples/vrai faux</option>
                                            <option value='shortanswers'>Réponses courtes/appariement</option>
                                        </select>
                                    </div>
                                    <div class="form-group row">
                                        <label for="nb_results" class="col-sm-8 col-form-label">Nombre de
                                            résultats</label>
                                        <div class="col-sm-4">
                                            <input type="number" name="nb_results" class="form-control" id="nb_results"
                                                max="100" min="1" value="10">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" id="answer_type" name="answer_type">
                                            <option selected disabled>--Trier les résultats par--</option>
                                            <option value='order_by_1'>Trier par date mise à jour décroissante</option>
                                            <option value='order_by_1'>Trier par daate création décroissante</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Filter</button>
                                </div>
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="keywords" class="col-sm-2 col-form-label">Mot clé</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="keywords" class="form-control" id="keywords"
                                                placeholder="reference, id, ou tag associé aux questions"
                                                autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Période de création:</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="far fa-calendar-alt"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control float-right" id="create_between"
                                                name="create_between">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                        <label>Période de mise à jour:</label>

                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="far fa-calendar-alt"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control float-right" id="update_between"
                                                name="update_between">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div> <!-- from row end-->

                            <div class="row">
                                <div class="pt-3">
                                    <a href="{{ route('hhquiz.quizzes.create.form') }}"
                                        class="btn btn-secondary  mb-3">Retourner au Quiz</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->

            <!-- right column -->
            <div class="col-md-6">
                <!-- jquery validation -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Les questions</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->

                    <div class="card-body">
                        <!-- form start -->
                        <form role="form" id="quickForm" novalidate="novalidate" method="POST" action="#la-route">
                            <div class="row">
                                <div class="col">
                                    hello

                                </div>
                            </div> <!-- from row end-->
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (right) -->
        </div>
        @stop

            @section('css')
            <link rel="stylesheet" href="{{ asset('vendor/summernote/summernote-bs4.min.css') }}">
            <link rel="stylesheet"
                href="{{ asset('vendor/daterangepicker/daterangepicker.css') }}">
            @stop

                @push('js')
                    <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}">
                    </script>
                    <script
                        src="{{ asset('vendor/jquery-validation/additional-methods.min.js') }}">
                    </script>
                    <script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
                    <script src="{{ asset('vendor/daterangepicker/moment.min.js') }}"></script>
                    <script src="{{ asset('vendor/daterangepicker/daterangepicker.js') }}">
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $(function () {
                                //Date range picker
                                $("#update_between,#create_between").each(function () {
                                    $(this).daterangepicker({
                                        "locale": {
                                            "format": "DD/MM/YYYY",
                                            "separator": " - ",
                                            "applyLabel": "Valider",
                                            "cancelLabel": "Annuler",
                                            "fromLabel": "De",
                                            "toLabel": "A",
                                            "daysOfWeek": [
                                                "Dim",
                                                "Lun",
                                                "Mar",
                                                "Mer",
                                                "Jeu",
                                                "Ven",
                                                "Sam"
                                            ],
                                            "monthNames": [
                                                "Janv",
                                                "Fév",
                                                "Mar",
                                                "Avr",
                                                "Mai",
                                                "Jun",
                                                "Jui",
                                                "Aou",
                                                "Sept",
                                                "Oct",
                                                "Nov",
                                                "Dec"
                                            ],
                                            "firstDay": 1
                                        },
                                        timePicker: false
                                    });
                                });
                            });
                            $('.textarea').summernote();
                            jQuery.validator.setDefaults({
                                // This will ignore all hidden elements alongside `contenteditable` elements
                                // that have no `name` attribute
                                //https://stackoverflow.com/questions/42086841/jquery-validate-with-summernote-editor-error-cannot-read-property-replace-of
                                //https://github.com/jquery-validation/jquery-validation/issues/1875
                                ignore: ":hidden, [contenteditable='true']:not([name])"
                            });


                            $('#quickForm').validate({
                                /*rules: {
                                    title: {
                                        required: true,
                                        minlength: 1
                                    },
                                },
                                messages: {
                                    title: {
                                        required: "Veuillez rentrer le nom du quiz",
                                        minlength: "Veuillez saisir au moins 4 caractères",
                                    }
                                },*/
                                errorElement: 'span',
                                errorPlacement: function (error, element) {
                                    error.addClass('invalid-feedback');
                                    element.closest('.form-group').append(error);
                                },
                                highlight: function (element, errorClass, validClass) {
                                    $(element).addClass('is-invalid');
                                },
                                unhighlight: function (element, errorClass, validClass) {
                                    $(element).removeClass('is-invalid');
                                }
                            });
                        });

                    </script>
                @endpush
