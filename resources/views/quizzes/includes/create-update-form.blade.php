@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
@endif
<form role="form" id="quickForm" novalidate="novalidate" method="POST"
    action="{{ route('hhquiz.quizzes.create.post') }}">
    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="title">Nom</label>
            <input required type="text" name="title" class="form-control"
                value="{{ request()->input('title') }}" id="title"
                placeholder="Taper le nom du quiz ici" autocomplete="off">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" name="description" class="form-control" id="description"
                placeholder="Taper une description du quiz ici" autocomplete="off">
        </div>
        <div class="form-group">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="is_enabled" name="is_enabled">
                <label class="custom-control-label" for="is_enabled">Actif ?</label>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Valider</button>
    </div>
</form>
