<script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-validation/additional-methods.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#quickForm').validate({
      rules: {
        title: {
          required: true,
          minlength: 4
        },
      },
      messages: {
        title: {
          required: "Veuillez rentrer le nom du quiz",
          minlength: "Veuillez saisir au moins 4 caractères",
        }
      },
      errorElement: 'span',
      errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>