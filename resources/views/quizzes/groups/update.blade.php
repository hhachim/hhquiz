@extends('hhquiz::layouts.quizzes.groups.form')
@php
  $title 		=  'Mise à jour d\'un classeur de quiz';
  $url = route('hhquiz.quizzes.groups.update.put', ['uuid' => request('uuid')]);
  $methode = 'PUT';
@endphp