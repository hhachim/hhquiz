@extends('hhquiz::layouts.quizzes.groups.form')
@php
  $title 		=  'Création d\'un classeur de quiz';
  $url = route('hhquiz.quizzes.groups.create.post');
  $methode = 'POST';
@endphp