@extends('adminlte::page')

@section('title', 'Les Quiz')

@section('content_header')
<h1>Quiz : {{ $quiz->title }} </h1>
@stop

@section('content')

    <div class="row">
        <div class="col-md-7">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-info"></i>
                        Information
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <dl>
                        <dt>Description : {{ $quiz->description }}</dt>
                    </dl>
                    <table class="table table-sm table-striped">
                        <tbody>
                            <tr>
                                <td>Id :</td>
                                <td>{{ $quiz->id }}</td>
                            </tr>
                            <tr>
                                <td>Etat :</td>
                                <td>
                                    @if ($quiz->is_enabled == 1)
                                        <span class="badge badge-success">Actif</span>
                                    @else 
                                        <span class="badge badge-danger">Inactif</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Afficher en priorité les questions où le même élève à échoué ?</td>
                                <td>
                                    @if ($quiz->select_user_failed_questions_first == 1)
                                        <span class="badge badge-success">Oui</span>
                                    @else 
                                        <span class="badge badge-danger">Non</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Ce quiz doit être fait après :</td>
                                <td>
                                    @if (isset($quiz->requiredQuiz->id ))
                                <a href=''>Quiz n° {{ $quiz->requiredQuiz->uuid }} :</a> {{ $quiz->requiredQuiz->title }}
                                    @else 
                                        A tout moment
                                    @endif
                                </td>
                            </tr>
                            @if (isset($quiz->requiredQuiz->id ))
                            <tr>
                                <td>Note minimum requise pour le quiz n° {{$quiz->requiredQuiz->id}} :</td>
                                <td>
                                    {{ $quiz->required_quiz_percent_note }}%
                                </td>
                            </tr>
                            @endif
                            <tr>
                                <td>Créé le :</td>
                                <td>{{ $quiz->created_at }}</td>
                            </tr>
                            <tr>
                                <td>Mis à jour le :</td>
                                <td>{{ $quiz->updated_at }}</td>
                            </tr>
                            <tr>
                                <td>Créé par :</td>
                                <td>{{ $quiz->author->name }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <a class="btn btn-info btn-sm" href="{{ route('hhquiz.quizzes.update.form', $quiz->uuid) }}">
                        <i class="fas fa-edit">
                        </i>
                        Modifier
                    </a>
                    <form action="#" method="POST" onsubmit="return confirm('Message confirmation');" style="display: inline-block;">
                          <input type="hidden" name="_method" value="DELETE">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"> Supprimer</i></button>
                    </form>
                    <div class="dropdown float-left">
                        <a class="btn btn-sm btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-list">
                        </i> 
                         Ajouter des questions
                        </a>                      
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="{{ route('hhquiz.questions.create.form') }}?quiz_uuid={{$quiz->uuid}}">Nouvelle question à créer</a>
                            <a class="dropdown-item" href="{{ route('hhquiz.quizzes.questions.create.form',$quiz->uuid)}}">Depuis la liste des questions déjà disponibles</a>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-md-5">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-link"></i>
                        Raccourcis
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <tbody>
                            <tr>
                                <td width='50%'><a href="">Tous les Quiz</a></td>
                                <td><a href="">Les Quiz du même classeur</a></td>
                            </tr>
                            <tr>
                                <td><a href="">Statistiques sur ce Quiz</a></td>
                                <td><a href="">Les Quiz du même auteur</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card info-->

            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-list"></i>
                        Ajout de questions depuis un classeur
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                          <i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <tbody>
                            <tr>
                                <td width='50%'><a href="">Tous les Quiz</a></td>
                                <td><a href="">Les Quiz du même classeur</a></td>
                            </tr>
                            <tr>
                                <td><a href="">Statistiques sur ce Quiz</a></td>
                                <td><a href="">Les Quiz du même auteur</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card question-->
        </div>
        <!-- ./col -->
    </div>

    @if(count($quiz->questions) > 0)
        @foreach ($quiz->questions as $question)
        <div class="row">
            <div class="col-md-12">
                <div class="card card-light">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-list"></i>
                        <a href="{{ route('hhquiz.questions.get.one', $question->uuid)}}">Question n°{{$question->id}} : {{ $question->title}}</a>
                            <a class="btn btn-light btn-sm" href="{{ route('hhquiz.questions.get.one', $question->uuid)}}">
                                <i class="fas fa-eye">
                                </i>
                                <i class="fas fa-edit">
                                </i>
                            </a>
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                              <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-7">
                                {{ $question->content}}                                
                            </div>
                            <div class="col-5">
                                @if( $question->answer_type =='multichoices' )
                                <table class="table table-sm">
                                    <thead>
                                      <tr>
                                        <th>Choix multiples</th>
                                        <th style="width: 40px"></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($question->choices as $choice)
                                            <tr>
                                                <td>{{$choice->content}}</td>
                                                <td>
                                                    @if($choice->is_good_answer)
                                                        <span class="badge bg-success">Oui</span>
                                                    @else
                                                        <span class="badge bg-danger">Non</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                  </table>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        @endforeach
    @endif

@stop