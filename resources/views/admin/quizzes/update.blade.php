@extends('hhquiz::layouts.quizzes.form')
@php
  $title 		=  'Mise à jour du Quiz';
  $url = route('hhquiz.quizzes.update.put', ['uuid' => request('uuid')]);
  $methode = 'PUT';
@endphp