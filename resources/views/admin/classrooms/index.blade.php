@extends('adminlte::page')

@section('title', 'Classeurs de sujets')

@section('content_header')
<h1>Liste des classes</h1>
@stop

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                  <table
                    id="table"
                    data-locale="fr-FR"
                    data-page-size="200"
                    data-toolbar="#toolbar"
                    data-search="true"
                    data-search-accent-neutralise="true"
                    data-show-refresh="true"
                    data-show-toggle="true"
                    data-show-fullscreen="true"
                    data-show-columns="true"
                    data-show-columns-toggle-all="false"
                    data-detail-view="true"
                    data-show-export="true"
                    data-click-to-select="true"
                    data-detail-formatter="detailFormatter"
                    data-minimum-count-columns="2"
                    data-show-pagination-switch="true"
                    data-pagination="true"
                    data-id-field="id"
                    data-page-list="[10, 25, 50, 100, all]"
                    data-show-footer="false"
                    data-side-pagination="false"
                    data-url="{{ route('hhquiz.admin.ajax.json.classrooms.search') }}?p=pp"
                    data-response-handler="responseHandler"
                    >
                  </table>
                  
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@stop

@section('css')
<link href="https://unpkg.com/bootstrap-table@1.17.1/dist/bootstrap-table.min.css" rel="stylesheet">
@stop

@section('js')
<script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.17.1/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.17.1/dist/bootstrap-table-locale-all.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.17.1/dist/extensions/export/bootstrap-table-export.min.js"></script>

<script>

  var $table = $('#table')
  var $remove = $('#remove')
  var selections = []

  function getIdSelections() {
    return $.map($table.bootstrapTable('getSelections'), function (row) {
      return row.id
    })
  }

  function responseHandler(res) {
    $.each(res.rows, function (i, row) {
      row.state = $.inArray(row.id, selections) !== -1;
      //row.name = '[' + row.name + ']';
      row.name=row.id;
    })
    return res
  }

  function detailFormatter(index, row) {
    var html = []
    $.each(row, function (key, value) {
      html.push('<p><b>' + key + ':</b> ' + value + '</p>')
    })
    return html.join('')
  }

  function operateFormatter(value, row, index) {
    return [
      '<a class="show btn btn-primary btn-xs" href="javascript:void(0)">',
      '<i class="fas fa-eye"></i>',
      '</a> ',
      '<a class="btn btn-secondary btn-xs" href="javascript:void(0)" title="Like">',
      '<i class="fa fa-edit"></i>',
      '</a>  ',
      '<a class="remove btn btn-danger btn-xs" href="javascript:void(0)" title="Remove">',
      '<i class="fa fa-trash"></i>',
      '</a>'
    ].join('')
  }

  window.operateEvents = {
    'click .show': function (e, value, row, index) {
      window.location.href = window.location.pathname + '/' + row.uuid;
      //console.log('show:',showUrl);
      //alert('You click like action, row: ' + JSON.stringify(row))
    },
    'click .remove': function (e, value, row, index) {
      $table.bootstrapTable('remove', {
        field: 'id',
        values: [row.id]
      })
    }
  }

  function initTable() {
    $table.bootstrapTable('destroy').bootstrapTable({
      height: 550,
      locale: 'fr-FR',
      columns: [
        /*{
          field: 'state',
          checkbox: true,
          align: 'center',
          valign: 'middle'
        },
        */ 
        {
          field: 'actions',
          title: 'Action',
          align: 'center',
          clickToSelect: false,
          events: window.operateEvents,
          formatter: operateFormatter
        },  {
          title: 'Id',
          field: 'id',
          //align: 'center',
          //valign: 'middle',
          sortable: true
        }, {
          title: 'Title',
          field: 'title',
          align: 'left',
          sortable: true
        }, {
          title: 'Créée par',
          field: 'author.name',
          align: 'left',
          sortable: true
        }, {
          title: 'Nb Quiz',
          field: 'cache_nb_quizzes',
          sortable: true,
        }, {
          title: 'Nb Questions',
          field: 'cache_nb_questions',
          sortable: true,
        }, {
          title: 'Nb Matières',
          field: 'cache_nb_subjects',
          sortable: true,
        }, {
          title: 'Nb Chapitres',
          field: 'cache_nb_chapters',
          sortable: true,
        }, {
          title: 'Nb Sous-chapitres',
          field: 'cache_nb_sub_chapters',
          sortable: true,
        }, {
          title: 'Nb Elèves',
          field: 'cache_nb_students',
          sortable: true,
        }, {
          title: 'Date Cache',
          field: 'cache_updated_at',
          sortable: true,
        }, 
      ]
    })
    $table.on('check.bs.table uncheck.bs.table ' +
      'check-all.bs.table uncheck-all.bs.table',
    function () {
      $remove.prop('disabled', !$table.bootstrapTable('getSelections').length)

      // save your data, here just save the current page
      selections = getIdSelections()
      // push or splice the selections if you want to save all data selections
    })
    $table.on('all.bs.table', function (e, name, args) {
      //console.log(name, args)
    })
    $remove.click(function () {
      var ids = getIdSelections()
      $table.bootstrapTable('remove', {
        field: 'id',
        values: ids
      })
      $remove.prop('disabled', true)
    })
  }


$(function() {
    initTable();
});
  </script>
@stop