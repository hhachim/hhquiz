@extends('adminlte::page')

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ $title }}</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
            </div>
            </div>
            <!-- /.card-header -->

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <div class="card-body">
            <!-- form start -->
            <form role="form" id="quickForm" novalidate="novalidate" method="POST"
                action="{{ $url }}">
                <input type="hidden" name="_method" value="{{ $methode }}">
                @if( !empty( Request::get('quiz_uuid') ) )
                    <input type="hidden" name="quiz_uuid" value="{{ Request::get('quiz_uuid') }}">
                @endif
                @csrf                
                    <div class="form-group">
                        <label for="title">Nom</label>
                        <input required type="text" name="title" class="form-control"
                            @isset( $question->title )
                            value="{{ $question->title }}" 
                            @endisset
                            id="title"
                            placeholder="Taper le nom du quiz ici" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="content">Contenu</label>
                        <textarea class="textarea" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="content" id="content">
                            @isset( $question->content )
                            {{ $question->content }} 
                            @endisset
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="keywords">Mots clés</label>
                        <input type="text" name="keywords" class="form-control" id="keywords"
                            @isset( $question->keywords )
                            value="{{ $question->keywords }}" 
                            @endisset
                            placeholder="Mots clés, séparés par une virgule" autocomplete="off">
                    </div>
                    @if (isset($groups) && count($groups) > 0)
                    <div class="form-group">
                        <label for="group_id">Classeur</label>
                        <select class="form-control" id="group_id" name="group_id">
                            <option value=''>Aucun</option>
                            @foreach ($groups as $group)
                                <option value= "{{ $group->id }}" 
                                    @if (isset($question) && $question->group_id == $group->id)
                                    selected
                                    @endif
                                    >
                                    @isset($group->parent->parent->parent->title)
                                    {{ $group->parent->parent->parent->title }}/
                                    @endisset
                                    @isset($group->parent->parent->title)
                                    {{ $group->parent->parent->title }}/
                                    @endisset
                                    @isset($group->parent->title)
                                    {{ $group->parent->title }}/
                                    @endisset
                                    @isset($group->title)
                                    {{ $group->title }}
                                    @endisset
                                </option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="answer_type">Réponses attendues</label>
                        <select class="form-control" id="answer_type" name="answer_type">
                            <option value='multichoices'
                            @if (isset($question) && $question->answer_type =='multichoices')
                                selected
                            @endif
                            >Choix multiples</option>
                            <option value='shortanswers'
                            @if (isset($question) && $question->answer_type =='shortanswers')
                                selected
                            @endif
                            >Réponses courtes</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" 
                            @isset( $question->is_enabled )
                                @if( $question->is_enabled ==1)
                                checked
                                @endif
                            @endisset 
                            class="custom-control-input" id="is_enabled" name="is_enabled">
                            <label class="custom-control-label" for="is_enabled">Actif ?</label>
                        </div>
                    </div>
                        <button type="submit" class="btn btn-primary">Valider</button>
                </form>
                </div>
                <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
</div>
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/summernote/summernote-bs4.min.css') }}">
@stop

@section('js')
<script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.textarea').summernote();
        jQuery.validator.setDefaults({
  // This will ignore all hidden elements alongside `contenteditable` elements
  // that have no `name` attribute
  //https://stackoverflow.com/questions/42086841/jquery-validate-with-summernote-editor-error-cannot-read-property-replace-of
  //https://github.com/jquery-validation/jquery-validation/issues/1875
  ignore: ":hidden, [contenteditable='true']:not([name])"
});
       

        $('#quickForm').validate({
            rules: {
                title: {
                    required: true,
                    minlength: 1
                },
            },
            messages: {
                title: {
                    required: "Veuillez rentrer le nom du quiz",
                    minlength: "Veuillez saisir au moins 4 caractères",
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });

</script>
@stop
