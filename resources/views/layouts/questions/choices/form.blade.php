@extends('adminlte::page')

@section('content_header')
Quetion n°{{ $question->id }}: {{ $question->title }}, 
créée par {{ $question->author->name }}, 
le {{ $question->created_at }} 
    @if(!empty( $question->group->title ))
    , dans le classeur {{ $question->group->title }}
    @endif
@stop
@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ $title }}</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
            </div>
            </div>
            <!-- /.card-header -->

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <div class="card-body">
            <!-- form start -->
            <form role="form" id="quickForm" novalidate="novalidate" method="POST"
                action="{{ $url }}">
                <input type="hidden" name="_method" value="{{ $methode }}">
                @csrf                
                    <div class="form-group">
                        <label for="content">Contenu</label>
                        <input required type="text" name="content" class="form-control"
                            @isset( $choice->content )
                            value="{{ $choice->content }}" 
                            @endisset
                            id="content"
                            placeholder="Contenu du choix" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="feedback">Feedback</label>
                        <input type="text" name="feedback" class="form-control"
                            @isset( $choice->feedback )
                            value="{{ $choice->feedback }}" 
                            @endisset
                            id="feedback"
                            placeholder="Feedback du choix" autocomplete="off">
                    </div>
                    
                    <div class="form-group">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" 
                            @isset( $choice->is_good_answer )                            
                                @if( $choice->is_good_answer ==1)
                                checked
                                @endif
                            @endisset 
                            class="custom-control-input" id="is_good_answer" name="is_good_answer">
                            <label class="custom-control-label" for="is_good_answer">Ce choix est une bonne réponse?</label>
                        </div>
                    </div>
                        <button type="submit" class="btn btn-primary">Valider</button>
                </form>
                </div>
                <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
</div>
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/summernote/summernote-bs4.min.css') }}">
@stop

@section('js')
<script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.textarea').summernote();
        jQuery.validator.setDefaults({
  // This will ignore all hidden elements alongside `contenteditable` elements
  // that have no `name` attribute
  //https://stackoverflow.com/questions/42086841/jquery-validate-with-summernote-editor-error-cannot-read-property-replace-of
  //https://github.com/jquery-validation/jquery-validation/issues/1875
  ignore: ":hidden, [contenteditable='true']:not([name])"
});
       

        $('#quickForm').validate({
            rules: {
                content: {
                    required: true,
                    minlength: 1
                },
            },
            messages: {
                content: {
                    required: "Veuillez rentrer le nom du quiz",
                    minlength: "Veuillez saisir au moins 4 caractères",
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });

</script>
@stop
