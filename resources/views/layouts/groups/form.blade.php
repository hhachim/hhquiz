@extends('adminlte::page')

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ $title }}</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
            </div>
            </div>
            <!-- /.card-header -->

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <div class="card-body">
            <!-- form start -->
            <form role="form" id="quickForm" novalidate="novalidate" method="POST"
                action="{{ $url }}">
                <input type="hidden" name="_method" value="{{ $methode }}">
                @csrf                
                    <div class="form-group">
                        <label for="title">Nom</label>
                        <input required type="text" name="title" class="form-control"
                            @isset( $group->title )
                            value="{{ $group->title }}" 
                            @endisset
                            id="title"
                            placeholder="Taper le nom du quiz ici" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input required type="text" name="description" class="form-control"
                            @isset( $group->description )
                            value="{{ $group->description }}" 
                            @endisset
                            id="description"
                            placeholder="Description du groupe" autocomplete="off">
                    </div>
                    @if (count($parents) > 0)
                    <div class="form-group">
                        <label for="parent_id">Groupe Parent</label>
                        <select class="form-control" id="parent_id" name="parent_id">
                            <option value=''>Aucun</option>
                            @foreach ($parents as $parent)
                                <option value= "{{ $parent->id }}" 
                                    @if (isset($group) && $parent->id == $group->parent_id)
                                    selected
                                    @endif
                                    >
                                    @isset($parent->parent->parent->parent->title)
                                    {{ $parent->parent->parent->parent->title }}/
                                    @endisset
                                    @isset($parent->parent->parent->title)
                                    {{ $parent->parent->parent->title }}/
                                    @endisset
                                    @isset($parent->parent->title)
                                    {{ $parent->parent->title }}/
                                    @endisset
                                    @isset($parent->title)
                                    {{ $parent->title }}
                                    @endisset
                                </option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    <div class="form-group">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" 
                            @isset( $group->is_enabled )
                                @if( $group->is_enabled ==1)
                                checked
                                @endif
                            @endisset 
                            class="custom-control-input" id="is_enabled" name="is_enabled">
                            <label class="custom-control-label" for="is_enabled">Actif ?</label>
                        </div>
                    </div>
                        <button type="submit" class="btn btn-primary">Valider</button>
                </form>
                </div>
                <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
</div>
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/summernote/summernote-bs4.min.css') }}">
@stop

@section('js')
<script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.textarea').summernote();
        jQuery.validator.setDefaults({
  // This will ignore all hidden elements alongside `contenteditable` elements
  // that have no `name` attribute
  //https://stackoverflow.com/questions/42086841/jquery-validate-with-summernote-editor-error-cannot-read-property-replace-of
  //https://github.com/jquery-validation/jquery-validation/issues/1875
  ignore: ":hidden, [contenteditable='true']:not([name])"
});
       

        $('#quickForm').validate({
            rules: {
                title: {
                    required: true,
                    minlength: 1
                },
            },
            messages: {
                title: {
                    required: "Veuillez rentrer le nom du quiz",
                    minlength: "Veuillez saisir au moins 4 caractères",
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });

</script>
@stop
