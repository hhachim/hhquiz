@extends('adminlte::page')

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">{{ $title }}</h3>
            </div>
            <!-- /.card-header -->

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <!-- form start -->
            <form role="form" id="quickForm" novalidate="novalidate" method="POST"
                action="{{ $url }}">
                <input type="hidden" name="_method" value="{{ $methode }}">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Nom</label>
                        <input required type="text" name="title" class="form-control"
                            @isset( $quiz->title )
                            value="{{ $quiz->title }}" 
                            @endisset
                            id="title"
                            placeholder="Taper le nom du quiz ici" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" name="description" class="form-control" id="description"
                            @isset( $quiz->description )
                            value="{{ $quiz->description }}" 
                            @endisset
                            placeholder="Taper une description du quiz ici" autocomplete="off">
                    </div>
                    @if (isset($parents) && count($parents) > 0)
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="required_quiz_id">Ce quiz doit être fait après</label>
                            <select class="form-control" id="required_quiz_id" name="required_quiz_id">
                                <option value=''>à tout moment</option>
                                @foreach ($parents as $parent)
                                    <option value= "{{ $parent->id }}" 
                                        @if (isset($quiz) && $parent->id == $quiz->required_quiz_id)
                                        selected
                                        @endif
                                        >{{ $parent->title }} (n°{{$parent->id}})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="required_quiz_percent_note">Pourcentage note minimum du Quiz requis</label>
                            <input type="number" min="0" max="100" name="required_quiz_percent_note" class="form-control"
                                @isset( $quiz->required_quiz_percent_note )
                                value="{{ $quiz->required_quiz_percent_note }}" 
                                @endisset
                                id="required_quiz_percent_note"
                                placeholder="Pourcentage note minimale requise" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="classroom_id">Classe</label>
                            <select class="form-control" id="classroom_id" name="classroom_id">
                                <option value='' disabled>Veuillez choisir une classe</option>
                                @foreach ($parents as $parent)
                                    <option value= "{{ $parent->id }}" 
                                        @if (isset($quiz) && $parent->id == $quiz->required_quiz_id)
                                        selected
                                        @endif
                                        >{{ $parent->title }} (n°{{$parent->id}})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="subject_id">Matière</label>
                            <select class="form-control" id="subject_id" name="subject_id">
                                <option value='' disabled>---</option>
                                @foreach ($parents as $parent)
                                    <option value= "{{ $parent->id }}" 
                                        @if (isset($quiz) && $parent->id == $quiz->required_quiz_id)
                                        selected
                                        @endif
                                        >{{ $parent->title }} (n°{{$parent->id}})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="chapter_id">Chapitre</label>
                            <select class="form-control" id="chapter_id" name="chapter_id">
                                <option value='' disabled>---</option>
                                @foreach ($parents as $parent)
                                    <option value= "{{ $parent->id }}" 
                                        @if (isset($quiz) && $parent->id == $quiz->required_quiz_id)
                                        selected
                                        @endif
                                        >{{ $parent->title }} (n°{{$parent->id}})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="sub_chapter_id">Sous Chapitre</label>
                            <select class="form-control" id="sub_chapter_id" name="sub_chapter_id">
                                <option value='' disabled>---</option>
                                @foreach ($parents as $parent)
                                    <option value= "{{ $parent->id }}" 
                                        @if (isset($quiz) && $parent->id == $quiz->required_quiz_id)
                                        selected
                                        @endif
                                        >{{ $parent->title }} (n°{{$parent->id}})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" 
                            @isset( $quiz->select_user_failed_questions_first )
                                @if( $quiz->select_user_failed_questions_first ==1)
                                checked
                                @endif
                            @endisset 
                            class="custom-control-input" id="select_user_failed_questions_first" name="select_user_failed_questions_first">
                            <label class="custom-control-label" for="select_user_failed_questions_first">Pour un même élève, lui montrer en priorité les questions où il a échoué ?</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" 
                            @isset( $quiz->is_enabled )
                                @if( $quiz->is_enabled ==1)
                                checked
                                @endif
                            @endisset 
                            class="custom-control-input" id="is_enabled" name="is_enabled">
                            <label class="custom-control-label" for="is_enabled">Actif ?</label>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Valider</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
</div>
@stop

@section('js')
<script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-validation/additional-methods.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#quickForm').validate({
            rules: {
                title: {
                    required: true,
                    minlength: 1
                },
            },
            messages: {
                title: {
                    required: "Veuillez rentrer le nom du quiz",
                    minlength: "Veuillez saisir au moins 4 caractères",
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });

</script>
@stop
