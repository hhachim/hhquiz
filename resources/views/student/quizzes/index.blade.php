@extends('adminlte::page')

@section('title', 'Mes Quiz')

@section('content_header')
<h1>Mes Quiz</h1>
@stop

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">

            <div class="card-body">
                <table class="table table-hover datatable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nom</th>
                            <th>Status</th>
                            <th>Créé par</th>
                            <th>Créé le</th>
                            <th>Mise à jour le</th>
                            <th width="250">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($quizzes as $quiz)
                    <tr>
                        <td>{{$quiz->id}}</td>
                        <td><a href="">{{ $quiz->title }}</a></td>
                        <td>
                            @if ($quiz->is_enabled == 1)
                                <span class="badge badge-success">Actif</span>
                            @else 
                                <span class="badge badge-danger">Inactif</span>
                            @endif
                        </td>
                        <td>{{$quiz->author->name}}</td>
                        <td>{{$quiz->created_at}}</td>
                        <td>{{$quiz->updated_at}}</td>
                        <td class="project-actions text-right">
                            <a class="btn btn-primary btn-sm" href="">
                              <i class="fas fa-eye">
                              </i>
                              Voir
                            </a>
                            <a class="btn btn-info btn-sm" href="">
                              <i class="fas fa-edit">
                              </i>
                              Modifier
                            </a>
                            <form action="#" method="POST" onsubmit="return confirm('Message confirmation');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"> Suppr</i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@stop

@section('css')
<!--
<link rel="stylesheet" href="/css/admin_custom.css">
-->
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/datatables-plugins/responsive/css/responsive.bootstrap4.min.css') }}">
@stop

@section('js')
<script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    $(function() {
        $('.datatable').DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "ordering": true,
            "info": false,
            "autoWidth": true,
            "responsive": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json"
            }
        });
    });
</script>
@stop