@extends('adminlte::page')

@section('title', 'Les Groupes')

@section('content_header')
<h1>Les Groupes</h1>
@stop

@section('content')


<div class="row">
    <div class="col-12">
        <div class="card">
            
            <div class="card-header">
                <a href="{{ route('hhquiz.groups.create.form') }}" class="btn btn-success">Créer une groupe</a>
                <div class="card-tools"> 
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
                <div class="input-group-prepend float-right">
                    <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      Filtre dates
                    </button>
                    <ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 48px, 0px);">
                      <li class="dropdown-item"><a href="#">Action</a></li>
                      <li class="dropdown-item"><a href="#">Another action</a></li>
                      <li class="dropdown-item"><a href="#">Something else here</a></li>
                      <li class="dropdown-divider"></li>
                      <li class="dropdown-item"><a href="#">Separated link</a></li>
                    </ul>
                  </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered table-hover datatable">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Nom</td>
                            <td>Status</td>
                            <td>Créé par</td>
                            <td>Créé le</td>
                            <td>Mise à jour le</td>
                            <td width="250">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($groups as $group)
                    <tr>
                        <td>{{$group->id}}</td>
                        <td>
                            @isset($group->parent->parent->title)
                            <small><a href="{{ route('hhquiz.groups.get.one', $group->parent->parent->id) }}">{{ $group->parent->parent->title }} /</a></small> 
                            @endisset
                            @isset($group->parent->title)
                            <small><a href="{{ route('hhquiz.groups.get.one', $group->parent->id) }}">{{ $group->parent->title }} /</a></small> 
                            @endisset
                            {{$group->title}}
                        </td>
                        <td>
                            @if ($group->is_enabled == 1)
                                <span class="badge badge-success">Actif</span>
                            @else 
                                <span class="badge badge-danger">Inactif</span>
                            @endif
                        </td>
                        <td>{{$group->author->name}}</td>
                        <td>{{$group->created_at}}</td>
                        <td>{{$group->updated_at}}</td>
                        <td class="project-actions text-right">
                            <a class="btn btn-primary btn-sm" href="{{ route('hhquiz.groups.get.one', $group->id) }}">
                              <i class="fas fa-eye">
                              </i>
                              Voir
                            </a>
                            <a class="btn btn-info btn-sm" href="{{ route('hhquiz.groups.update.form', $group->id) }}">
                              <i class="fas fa-edit">
                              </i>
                              Modifier
                            </a>
                            <form action="#" method="POST" onsubmit="return confirm('Message confirmation');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"> Supprimer</i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                        <td>ID</td>
                        <td>Nom</td>
                        <td>Status</td>
                        <td>Créé par</td>
                        <td>Créé le</td>
                        <td>Mise à jour le</td>
                        <td>Actions</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@stop

@section('css')
<!--
<link rel="stylesheet" href="/css/admin_custom.css">
-->
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/datatables-plugins/responsive/css/responsive.bootstrap4.min.css') }}">
@stop

@section('js')
<script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-plugins/responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-plugins/responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    $(function() {
        $('.datatable').DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "responsive": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json"
            }
        });
    });
</script>
@stop