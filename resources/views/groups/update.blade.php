@extends('hhquiz::layouts.groups.form')
@php
  $title 		=  'Mise à jour d\'un groupe';
  $url = route('hhquiz.groups.update.put', ['id' => request('id')]);
  $methode = 'PUT';
@endphp