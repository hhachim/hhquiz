<?php

return [
    'theme' => [
        'color' => [
            'info' => 'info',
            'yes' => 'success',
            'no' => 'danger',
            'create' => 'success',
            'create-sub' => 'warning',
            'update' => 'info',
            'delete' => 'danger',
            'filter.datatable' => 'light',
        ],
    ],
    'pagination' => [
        'itemsPerPage' => 2
    ]
];
