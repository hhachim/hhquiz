<?php

// 'routes/web.php'

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
Route::group(['as' => 'hhquiz.'], function () {
    event(new Routing());
    Route::middleware(['web','admin.user'])->group(function () {
        Route::get('/admin/hhquiz', [IndexController::class, 'index'])->name('index');
    });

    Route::middleware(['web','admin.user'])->group(function () {
        Route::get('/admin/hhquiz/quizzes', [QuizzesController::class, 'index'])->name('quizzes.index');
    });

    event(new RoutingAfter());
});
*/

/*Route::prefix('admin')->middleware('auth')->group(['as' => 'hhquiz.'], function () {

});*/

Route::middleware(['web', 'auth'])
    ->name('hhquiz.admin.')
    ->namespace('Hachim\HHQuiz\Http\Controllers\Admin')
    ->group(function () {
        Route::get('/admin/hhquiz', 'HomeController@index')->name('home');
    });

Route::middleware(['web', 'auth'])
    ->prefix('admin/hhquiz')
    ->name('hhquiz.')
    ->namespace('Hachim\HHQuiz\Http\Controllers')
    ->group(function () {
        //classrooms
        Route::get('/classrooms', 'Admin\Classrooms\IndexController@index')->name('classrooms.index');
        Route::get('/classrooms/ajax/json/search', 'Admin\Classrooms\IndexController@indexAjax')
            ->name('admin.ajax.json.classrooms.search');


        //quizzes
        Route::get('/quizzes', 'Admin\Quizzes\IndexController@index')->name('quizzes.index');
        Route::get('/quizzes/ajax/json/search', 'Admin\Quizzes\IndexController@indexAjax')
            ->name('admin.ajax.json.quizzes.search');
        Route::get('/quizzes/{uuid}', 'Admin\Quizzes\IndexController@getOne')
            ->name('quizzes.get.one')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
        Route::get('/quizzes/{uuid}/update', 'Admin\Quizzes\IndexController@updateForm')
            ->name('quizzes.update.form')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
        Route::put('/quizzes/{uuid}/update', 'Admin\Quizzes\IndexController@update')
            ->name('quizzes.update.put')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
        Route::get('/quizzes/create', 'Admin\Quizzes\IndexController@createForm')
            ->name('quizzes.create.form');
        Route::post('/quizzes/create', 'Admin\Quizzes\IndexController@create')
            ->name('quizzes.create.post');


        /***************************
         * 
         * 
         * Avant (old)
         */
        //quizzes
        //Route::get('/quizzes', 'QuizzesController@index')->name('quizzes.index');


        //quiz_question
        Route::post(
            '/quizzes/{quizId}/questions',
            'QuizQuestionsController@create'
        )->name('quizzes.questions.create.post')
            ->where('quizId', '[0-9]+');
        Route::get(
            '/quizzes/{quizId}/questions',
            'QuizQuestionsController@createForm'
        )->name('quizzes.questions.create.form')
            ->where('quizId', '[0-9]+');

        //questions
        Route::get('/questions', 'QuestionsController@index')
            ->name('questions.index');
        Route::get('/questions/create', 'QuestionsController@createForm')
            ->name('questions.create.form');
        Route::post('/questions/create', 'QuestionsController@create')
            ->name('questions.create.post');
        Route::get('/questions/{uuid}/update', 'QuestionsController@updateForm')
            ->name('questions.update.form')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
        Route::put('/questions/{uuid}/update', 'QuestionsController@update')
            ->name('questions.update.put')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
        Route::get('/questions/{uuid}', 'QuestionsController@getOne')
            ->name('questions.get.one')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');

        //user_groups
        Route::get('/groups', 'GroupsController@index')->name('groups.index');
        Route::get('/groups/create', 'GroupsController@createForm')
            ->name('groups.create.form');
        Route::post('/groups/create', 'GroupsController@create')
            ->name('groups.create.post');
        Route::get('/groups/{id}/update', 'GroupsController@updateForm')
            ->name('groups.update.form')
            ->where('id', '[0-9]+');
        Route::put('/groups/{id}/update', 'GroupsController@update')
            ->name('groups.update.put')
            ->where('id', '[0-9]+');
        Route::get('/groups/{id}', 'GroupsController@getOne')
            ->name('groups.get.one')
            ->where('id', '[0-9]+');

        //question_groups
        //ajax
        Route::get('/ajax/json/questions/groups/search', 'AjaxJsonQuestionGroupsSearchController')
            ->name('ajax.json.questions.groups.search');
        Route::get('/questions/groups', 'QuestionGroupsController@index')
            ->name('questions.groups.index');
        Route::get('/questions/groups/create', 'QuestionGroupsController@createForm')
            ->name('questions.groups.create.form');
        Route::post('/questions/groups/create', 'QuestionGroupsController@create')
            ->name('questions.groups.create.post');
        Route::get(
            '/questions/groups/{uuid}/update',
            'QuestionGroupsController@updateForm'
        )->name('questions.groups.update.form')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
        Route::put(
            '/questions/groups/{uuid}/update',
            'QuestionGroupsController@update'
        )->name('questions.groups.update.put')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
        Route::get('/questions/groups/{uuid}', 'QuestionGroupsController@getOne')
            ->name('questions.groups.get.one')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');

        //question_choices
        Route::get(
            '/questions/{question}/choices/create',
            'QuestionChoicesController@createForm'
        )->name('questions.choices.create.form')
            ->where('question', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
        Route::post(
            '/questions/{question}/choices/create',
            'QuestionChoicesController@create'
        )->name('questions.choices.create.post')
            ->where('question', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
        Route::get(
            '/questions/{question}/choices/{choice}/update',
            'QuestionChoicesController@updateForm'
        )->name('questions.choices.update.form')
            ->where(['question' => '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}', 'choice' => '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}']);
        Route::put(
            '/questions/{question}/choices/{choice}/update',
            'QuestionChoicesController@update'
        )->name('questions.choices.update.put')
            ->where(['question' => '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}', 'choice' => '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}']);

        //quiz_groups
        //ajax
        Route::get('/ajax/json/quizzes/groups/search', 'AjaxJsonQuizGroupsSearchController')
            ->name('ajax.json.quizzes.groups.search');
        Route::get('/quizzes/groups', 'QuizGroupsController@index')
            ->name('quizzes.groups.index');
        Route::get('/quizzes/groups/create', 'QuizGroupsController@createForm')
            ->name('quizzes.groups.create.form');
        Route::post('/quizzes/groups/create', 'QuizGroupsController@create')
            ->name('quizzes.groups.create.post');
        Route::get(
            '/quizzes/groups/{uuid}/update',
            'QuizGroupsController@updateForm'
        )->name('quizzes.groups.update.form')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
        Route::put(
            '/quizzes/groups/{uuid}/update',
            'QuizGroupsController@update'
        )->name('quizzes.groups.update.put')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
        Route::get('/quizzes/groups/{uuid}', 'QuizGroupsController@getOne')
            ->name('quizzes.groups.get.one')
            ->where('uuid', '[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}');
    });


Route::middleware(['web', 'auth'])
    ->prefix('hhquiz/student-area')
    ->name('hhquiz.student.')
    ->namespace('Hachim\HHQuiz\Http\Controllers\Student')
    ->group(function () {
        //quizzes
        Route::get('/quizzes', 'QuizzesController@index')->name('quizzes.index');
        Route::get('/quizzes/{id}', 'QuizzesController@getOne')
            ->name('quizzes.get.one')
            ->where('id', '[0-9]+');
    });
